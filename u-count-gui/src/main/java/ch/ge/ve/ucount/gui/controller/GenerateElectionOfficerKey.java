/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.controller;

import ch.ge.ve.javafx.business.i18n.LanguageUtils;
import ch.ge.ve.javafx.gui.control.MessageContainer;
import ch.ge.ve.javafx.gui.control.PrivateKeyInputGroup;
import ch.ge.ve.javafx.gui.control.PublicKeyInputGroup;
import ch.ge.ve.javafx.gui.control.Step;
import ch.ge.ve.javafx.gui.control.StepperPane;
import ch.ge.ve.javafx.gui.utils.FileBrowserService;
import ch.ge.ve.javafx.gui.utils.TaskUtils;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.ucount.business.key.ElectionOfficerKeyService;
import ch.ge.ve.ucount.util.key.KeyShare;
import ch.ge.ve.ucount.util.key.KeySharingScheme;
import ch.ge.ve.ucount.util.key.SplitKeyPair;
import com.google.common.base.Stopwatch;
import com.jfoenix.controls.JFXTextField;
import java.text.MessageFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

/**
 * JavaFX Controller for the generation of the election officer key.
 */
@Component
public class GenerateElectionOfficerKey {
  private static final Logger logger = LoggerFactory.getLogger(GenerateElectionOfficerKey.class);

  private final ElectionOfficerKeyService       electionOfficerKeyService;
  private final ElectionOfficerKeyConfiguration electionOfficerKeyConfiguration;
  private final ApplicationState                applicationState;
  private final TaskExecutor                    taskExecutor;
  private final FileBrowserService              fileBrowserService;

  private List<KeyShare>      electionOfficerKeyShares;
  private EncryptionPublicKey publicElectionOfficerKey;
  private StepperPane         sharesStepper;

  @FXML
  private MessageContainer errorMessageContainer;

  @FXML
  private MessageContainer successMessageContainer;

  @FXML
  private Pane nbrOfSharesInputGroupBox;

  @FXML
  private Pane submitNbrOfSharesBox;

  @FXML
  private Pane privateKeyInputGroupsBox;

  @FXML
  private Pane submitPrivateKeyBox;

  @FXML
  private JFXTextField nbrOfSharesField;

  @FXML
  private JFXTextField minNbrOfSharesToRecoverField;

  @Autowired
  public GenerateElectionOfficerKey(ElectionOfficerKeyService electionOfficerKeyService,
                                    ElectionOfficerKeyConfiguration electionOfficerKeyConfiguration,
                                    FileBrowserService fileBrowserService,
                                    ApplicationState applicationState,
                                    TaskExecutor taskExecutor) {
    this.electionOfficerKeyService = electionOfficerKeyService;
    this.electionOfficerKeyConfiguration = electionOfficerKeyConfiguration;
    this.applicationState = applicationState;
    this.fileBrowserService = fileBrowserService;
    this.taskExecutor = taskExecutor;
  }

  /**
   * Validate the number of total and minimum shares and generate the election officer key and initialize the stepper.
   */
  @FXML
  public void generateElectionOfficerKey() {
    if (nbrOfSharesField.validate() &&
        minNbrOfSharesToRecoverField.validate()) {

      applicationState.setElectionOfficerKeyGenerationInProgress(true);

      triggerKeyGeneration();
    }
  }

  private void triggerKeyGeneration() {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() throws Exception {
        Stopwatch stopwatch = Stopwatch.createStarted();

        int nbrOfShares = Integer.valueOf(nbrOfSharesField.getText());
        int nbrOfSharesToRecover = Integer.valueOf(minNbrOfSharesToRecoverField.getText());
        KeySharingScheme scheme = KeySharingScheme.of(nbrOfSharesToRecover, nbrOfShares);
        SplitKeyPair electionOfficerKey = electionOfficerKeyService.generateElectionOfficerKey(scheme);

        electionOfficerKeyShares = new ArrayList<>(electionOfficerKey.getPrivateKeyShares());
        publicElectionOfficerKey = electionOfficerKey.getPublicKey();

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Election officer key generation failed.", event.getSource().getException());
      onKeyGenerationFailure(task);
    });
    task.setOnSucceeded(event -> onKeyGenerationSuccess());

    taskExecutor.execute(task);
  }

  private <T> void onKeyGenerationFailure(Task<T> task) {
    electionOfficerKeyShares = null;
    publicElectionOfficerKey = null;

    applicationState.setElectionOfficerKeyGenerationInProgress(false);
    errorMessageContainer.setMessage(
        TaskUtils.formatErrorMessage("generate-election-officer-key.key-generation-failed", task)
    );
  }

  private void onKeyGenerationSuccess() {
    int nbrOfShares = Integer.valueOf(nbrOfSharesField.getText());

    sharesStepper = new StepperPane();
    privateKeyInputGroupsBox.getChildren().add(sharesStepper);

    PublicKeyInputGroup publicKeyInputGroup = new PublicKeyInputGroup(fileBrowserService);
    publicKeyInputGroup.getStyleClass().add("card-content");
    String publicKeyStepLabel =
        LanguageUtils.getCurrentResourceBundle().getString("generate-election-officer-key.public-key-step-label");
    sharesStepper.getSteps().add(new Step(publicKeyStepLabel, publicKeyInputGroup));

    String stepLabelPattern = LanguageUtils.getCurrentResourceBundle()
                                           .getString("generate-election-officer-key.share-label");
    for (int i = 1; i <= nbrOfShares; i++) {
      Step step = new Step(MessageFormat.format(stepLabelPattern, Integer.toString(i)));
      sharesStepper.getSteps().add(step);
    }

    errorMessageContainer.dismiss();
    nbrOfSharesInputGroupBox.setVisible(false);
    nbrOfSharesInputGroupBox.setManaged(false);
    submitNbrOfSharesBox.setVisible(false);
    submitNbrOfSharesBox.setManaged(false);
    privateKeyInputGroupsBox.setVisible(true);
    privateKeyInputGroupsBox.setManaged(true);
    submitPrivateKeyBox.setVisible(true);
    submitPrivateKeyBox.setManaged(true);

    applicationState.setElectionOfficerKeyGenerationInProgress(false);
  }

  /**
   * Validate and persist the current share in the stepper and advance automatically to the next step, once the last key
   * share is persisted a success message will be displayed.
   */
  @FXML
  public void persistShareOrPublicKey() {
    if (sharesStepper.getCurrentPosition() == 0) {

      PublicKeyInputGroup publicKeyInputGroup =
          (PublicKeyInputGroup) sharesStepper.getSteps().get(
              sharesStepper.getCurrentPosition()
          ).getContent();

      if (publicKeyInputGroup.validate()) {
        applicationState.setElectionOfficerKeyGenerationInProgress(true);
        startPublicKeyPersistence(publicKeyInputGroup, publicElectionOfficerKey);
      }

    } else {

      PrivateKeyInputGroup currentShareInputGroup =
          (PrivateKeyInputGroup) sharesStepper.getSteps().get(
              sharesStepper.getCurrentPosition()
          ).getContent();

      if (currentShareInputGroup.validate()) {
        KeyShare currentShare = electionOfficerKeyShares.get(sharesStepper.getCurrentPosition() - 1);
        applicationState.setElectionOfficerKeyGenerationInProgress(true);
        startSharePersistence(currentShareInputGroup, currentShare);
      }

    }
  }

  private void startPublicKeyPersistence(PublicKeyInputGroup publicKeyInputGroup,
                                         EncryptionPublicKey publicElectionOfficerKey) {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() throws Exception {
        Stopwatch stopwatch = Stopwatch.createStarted();

        electionOfficerKeyService.storePublicKey(publicElectionOfficerKey,
                                                 publicKeyInputGroup.getDestinationPath());

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Key share persistence failed.", event.getSource().getException());
      applicationState.setElectionOfficerKeyGenerationInProgress(false);
      publicKeyInputGroup.setErrorMessage(
          TaskUtils.formatErrorMessage("generate-election-officer-key.public-key-persistence-failed", task)
      );
    });
    task.setOnSucceeded(event -> onPersistSuccess());

    taskExecutor.execute(task);
  }

  private void startSharePersistence(PrivateKeyInputGroup currentShareInputGroup, KeyShare currentShare) {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() throws Exception {
        Stopwatch stopwatch = Stopwatch.createStarted();

        electionOfficerKeyService.storeKeyShare(currentShare,
                                                currentShareInputGroup.getDestinationPath(),
                                                currentShareInputGroup.getPassphrase());

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Key share persistence failed.", event.getSource().getException());
      applicationState.setElectionOfficerKeyGenerationInProgress(false);
      currentShareInputGroup.setErrorMessage(
          TaskUtils.formatErrorMessage("generate-election-officer-key.share-persistence-failed", task)
      );
    });
    task.setOnSucceeded(event -> onPersistSuccess());

    taskExecutor.execute(task);
  }

  private void onPersistSuccess() {
    applicationState.setElectionOfficerKeyGenerationInProgress(false);

    if (sharesStepper.getHasNext()) {
      PrivateKeyInputGroup privateKeyInputGroup = new PrivateKeyInputGroup(fileBrowserService);
      privateKeyInputGroup.getStyleClass().add("card-content");

      sharesStepper.next();
      sharesStepper.getSteps().get(sharesStepper.getCurrentPosition()).setContent(privateKeyInputGroup);
    } else {
      onElectionOfficerKeyCreated();
    }
  }

  private void onElectionOfficerKeyCreated() {
    errorMessageContainer.dismiss();
    nbrOfSharesInputGroupBox.setVisible(false);
    nbrOfSharesInputGroupBox.setManaged(false);
    submitNbrOfSharesBox.setVisible(false);
    submitNbrOfSharesBox.setManaged(false);
    privateKeyInputGroupsBox.setVisible(false);
    privateKeyInputGroupsBox.setManaged(false);
    submitPrivateKeyBox.setVisible(false);
    submitPrivateKeyBox.setManaged(false);

    successMessageContainer.setMessage(
        LanguageUtils.getCurrentResourceBundle().getString("generate-election-officer-key.key-generation-success")
    );
  }

  public ElectionOfficerKeyConfiguration getElectionOfficerKeyConfiguration() {
    return electionOfficerKeyConfiguration;
  }
}
