/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.input;

import ch.ge.ve.javafx.business.i18n.LanguageUtils;
import ch.ge.ve.javafx.gui.control.Step;
import ch.ge.ve.javafx.gui.control.StepperPane;
import ch.ge.ve.javafx.gui.utils.FXMLLoaderUtils;
import ch.ge.ve.javafx.gui.utils.FileBrowserService;
import ch.ge.ve.javafx.gui.utils.TaskUtils;
import ch.ge.ve.protocol.core.model.EncryptionKeyPair;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.ucount.business.key.ElectionOfficerKeyService;
import ch.ge.ve.ucount.gui.controller.ApplicationState;
import ch.ge.ve.ucount.gui.controller.ElectionOfficerKeyConfiguration;
import ch.ge.ve.ucount.util.key.KeyShare;
import ch.ge.ve.ucount.util.key.KeySharingScheme;
import ch.ge.ve.ucount.util.key.SplitKeyPair;
import com.google.common.base.Stopwatch;
import com.jfoenix.controls.JFXTextField;
import java.text.MessageFormat;
import java.time.Duration;
import java.util.HashSet;
import java.util.Set;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;

/**
 * A JavaFX element that prompts the user to input the necessary information to recover the election officer key, i.e.:
 *
 * <ul>
 * <li>The total number of key shares.</li>
 * <li>The minimum number of key shares required to recover the key.</li>
 * <li>As much key shares as the specified minimum number that is required to recover the key (see
 * {@link KeyShareImportInputGroup}.</li>
 * </ul>
 */
public class ElectionOfficerKeyRecoveryInputGroup extends VBox {
  private static final Logger logger = LoggerFactory.getLogger(ElectionOfficerKeyRecoveryInputGroup.class);

  @FXML
  private Pane nbrOfSharesInputGroupBox;

  @FXML
  private Pane submitNbrOfSharesBox;

  @FXML
  private Pane keyShareInputBox;

  @FXML
  private Pane submitKeyShareBox;

  @FXML
  private JFXTextField nbrOfSharesField;

  @FXML
  private JFXTextField minNbrOfSharesToRecoverField;

  private StepperPane sharesStepper;

  private Set<KeyShare> keyShares;

  private final FileBrowserService                  fileBrowserService;
  private final ApplicationState                    applicationState;
  private final ElectionOfficerKeyService           electionOfficerKeyService;
  private final ElectionOfficerKeyConfiguration     electionOfficerKeyConfiguration;
  private final TaskExecutor                        taskExecutor;
  private final ObjectProperty<EncryptionPublicKey> electionOfficerPublicKey;
  private final ObjectProperty<EncryptionKeyPair>   electionOfficerKeyPair;
  private final ObjectProperty<EventHandler<Event>> onElectionOfficerKeyPairReady;

  public ElectionOfficerKeyRecoveryInputGroup(FileBrowserService fileBrowserService,
                                              ApplicationState applicationState,
                                              ElectionOfficerKeyService electionOfficerKeyService,
                                              ElectionOfficerKeyConfiguration electionOfficerKeyConfiguration,
                                              TaskExecutor taskExecutor) {
    this.fileBrowserService = fileBrowserService;
    this.applicationState = applicationState;
    this.electionOfficerKeyService = electionOfficerKeyService;
    this.taskExecutor = taskExecutor;
    this.electionOfficerKeyConfiguration = electionOfficerKeyConfiguration;

    this.electionOfficerPublicKey = new SimpleObjectProperty<>(this, "electionOfficerPublicKey");
    this.electionOfficerKeyPair = new SimpleObjectProperty<>(this, "electionOfficerKeyPair");
    this.onElectionOfficerKeyPairReady = new SimpleObjectProperty<>(this, "onElectionOfficerKeyPairReady");

    FXMLLoaderUtils.load(this, "/view/ElectionOfficerKeyRecoveryInputGroup.fxml");
  }

  public ElectionOfficerKeyConfiguration getElectionOfficerKeyConfiguration() {
    return electionOfficerKeyConfiguration;
  }

  /**
   * Validate the number of total and minimum shares, if the validation is successful this method initialize the stepper
   * to recover the election office key.
   */
  @FXML
  public void submitNbrOfShares() {
    if (nbrOfSharesField.validate() && minNbrOfSharesToRecoverField.validate()) {
      int minNbrOfSharesToRecover = Integer.valueOf(minNbrOfSharesToRecoverField.getText());

      sharesStepper = new StepperPane();
      keyShareInputBox.getChildren().add(sharesStepper);

      String stepLabelPattern = LanguageUtils.getCurrentResourceBundle()
                                             .getString("election-officer-key-recovery-input-group.share-label");
      for (int i = 1; i <= minNbrOfSharesToRecover; i++) {
        KeyShareImportInputGroup keyShareImportInputGroup =
            new KeyShareImportInputGroup(fileBrowserService, electionOfficerPublicKey.get());
        keyShareImportInputGroup.getStyleClass().add("card-content");
        String stepLabel = MessageFormat.format(stepLabelPattern, Integer.toString(i));
        Step step = new Step(stepLabel, keyShareImportInputGroup);
        sharesStepper.getSteps().add(step);
      }

      nbrOfSharesInputGroupBox.setVisible(false);
      nbrOfSharesInputGroupBox.setManaged(false);
      submitNbrOfSharesBox.setVisible(false);
      submitNbrOfSharesBox.setManaged(false);
      keyShareInputBox.setVisible(true);
      keyShareInputBox.setManaged(true);
      submitKeyShareBox.setVisible(true);
      submitKeyShareBox.setManaged(true);

      keyShares = new HashSet<>(minNbrOfSharesToRecover);
    }
  }

  /**
   * Submit the current step and verify the integrity of the key share using the current {@link
   * #electionOfficerPublicKeyProperty()}. If the final key share is submitted and successfully validated then the
   * election officer key recovery task will be triggered.
   *
   * @see KeyShareImportInputGroup
   */
  @FXML
  public void submitCurrentKeyShare() {
    KeyShareImportInputGroup currentShareInputGroup = getCurrentKeyShareImportInputGroup();
    if (currentShareInputGroup.validate()) {
      applicationState.setElectionOfficerKeyRecoveryInProgress(true);
      readKeyShare(currentShareInputGroup);
    }
  }

  private KeyShareImportInputGroup getCurrentKeyShareImportInputGroup() {
    return (KeyShareImportInputGroup) sharesStepper.getSteps().get(sharesStepper.getCurrentPosition()).getContent();
  }

  private void readKeyShare(KeyShareImportInputGroup keyShareInputGroup) {
    Task<KeyShare> task = new Task<>() {
      @Override
      protected KeyShare call() throws Exception {
        return electionOfficerKeyService.readKeyShare(keyShareInputGroup.getKeySharePath(),
                                                      keyShareInputGroup.getPassphrase());
      }
    };

    task.setOnFailed(event -> {
      showErrorMessage(task, keyShareInputGroup);
      applicationState.setElectionOfficerKeyRecoveryInProgress(false);
      logger.error("Failed to recover key share.", event.getSource().getException());
    });

    task.setOnSucceeded(event -> {
      applicationState.setElectionOfficerKeyRecoveryInProgress(false);
      KeyShare currentKeyShare = task.getValue();
      if (keyShares.contains(currentKeyShare)) {
        keyShareInputGroup.setErrorMessage(
            LanguageUtils.getCurrentResourceBundle()
                         .getString("election-officer-key-recovery-input-group.duplicate-key-share")
        );
        getCurrentKeyShareImportInputGroup().clear();
      } else {
        keyShares.add(currentKeyShare);
        showNextStepOrRecoverKey(keyShareInputGroup);
      }
    });

    taskExecutor.execute(task);
  }

  private <T> void showErrorMessage(Task<T> task, KeyShareImportInputGroup keyShareInputGroup) {
    keyShareInputGroup.setErrorMessage(
        TaskUtils.formatErrorMessage("election-officer-key-recovery-input-group.key-share-import-failed", task)
    );
  }

  private void showNextStepOrRecoverKey(KeyShareImportInputGroup keyShareInputGroup) {
    if (sharesStepper.getHasNext()) {
      sharesStepper.next();
    } else {
      recoverElectionOfficerKeyPair(keyShareInputGroup);
    }
  }

  private void recoverElectionOfficerKeyPair(KeyShareImportInputGroup keyShareInputGroup) {
    applicationState.setElectionOfficerKeyRecoveryInProgress(true);

    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() throws Exception {
        Stopwatch stopwatch = Stopwatch.createStarted();

        electionOfficerKeyPair.set(
            electionOfficerKeyService.recoverKeyPair(createKeySharingScheme(), createSplitKeyPair())
        );

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      showErrorMessage(task, keyShareInputGroup);
      applicationState.setElectionOfficerKeyRecoveryInProgress(false);
      logger.error("Election officer key recovery failed.", event.getSource().getException());
    });

    task.setOnSucceeded(event -> {
      applicationState.setElectionOfficerKeyRecoveryInProgress(false);
      onElectionOfficerKeyPairReady.get().handle(event);
    });

    taskExecutor.execute(task);
  }

  private KeySharingScheme createKeySharingScheme() {
    int nbrOfShares = Integer.valueOf(nbrOfSharesField.getText());
    int minNbrOfSharesToRecover = Integer.valueOf(minNbrOfSharesToRecoverField.getText());

    return KeySharingScheme.of(minNbrOfSharesToRecover, nbrOfShares);
  }

  private SplitKeyPair createSplitKeyPair() {
    return new SplitKeyPair(electionOfficerPublicKey.get(), keyShares);
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Get the election officer public key that is used to verify the integrity of the key shares.
   *
   * @return the election officer public key.
   */
  public final EncryptionPublicKey getElectionOfficerPublicKey() {
    return electionOfficerPublicKey.get();
  }

  /**
   * Set the election officer public key that will be used to verify the integrity of the key shares.
   *
   * @param value the election officer public key.
   */
  public final void setElectionOfficerPublicKey(EncryptionPublicKey value) {
    electionOfficerPublicKey.setValue(value);
  }

  /**
   * The election officer public key property.
   *
   * @return the election officer public key property.
   */
  public final ObjectProperty<EncryptionPublicKey> electionOfficerPublicKeyProperty() {
    return electionOfficerPublicKey;
  }

  /**
   * Get the recovered election officer key pair.
   *
   * @return the election officer key pair.
   */
  public final EncryptionKeyPair getElectionOfficerKeyPair() {
    return electionOfficerKeyPair.get();
  }

  /**
   * The election officer key pair property.
   *
   * @return The election officer key pair property.
   */
  public final ObjectProperty<EncryptionKeyPair> electionOfficerKeyPairProperty() {
    return electionOfficerKeyPair;
  }

  /**
   * Set the current <code>onElectionOfficerKeyPairReady</code> callback. See {@link
   * #onElectionOfficerKeyPairReadyProperty()},
   *
   * @param value the callback.
   */
  public final void setOnElectionOfficerKeyPairReady(EventHandler<Event> value) {
    onElectionOfficerKeyPairReady.set(value);
  }

  /**
   * Get the current <code>onElectionOfficerKeyPairReady</code> callback. See {@link
   * #onElectionOfficerKeyPairReadyProperty()}.
   *
   * @return the current <code>onElectionOfficerKeyPairReady</code> callback.
   */
  public final EventHandler<Event> getOnElectionOfficerKeyPairReady() {
    return onElectionOfficerKeyPairReady.get();
  }

  /**
   * The property of the callback that is triggered once the election officer key has been successfully recovered and
   * {@link #electionOfficerKeyPairProperty()} can be safely retrieved.
   *
   * @return the <code>onElectionOfficerKeyPairReady</code> callback property.
   */
  public final ObjectProperty<EventHandler<Event>> onElectionOfficerKeyPairReadyProperty() {
    return onElectionOfficerKeyPairReady;
  }
}
