/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.validator;

import com.google.common.base.Strings;
import com.jfoenix.validation.base.ValidatorBase;
import java.text.MessageFormat;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextInputControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Validates that the number of shares necessary to recover the private key is smaller than the total number of shares.
 */
public class NbrOfSharesToRecoverValidator extends ValidatorBase {
  private static final Logger logger = LoggerFactory.getLogger(NbrOfSharesToRecoverValidator.class);

  private final StringProperty baseMessage;
  private final StringProperty nbrOfSharesText;

  /**
   * Create an empty number o shares to recover validator
   */
  public NbrOfSharesToRecoverValidator() {
    this(null);
  }

  /**
   * Create a number o shares to recover validator with the given default message.
   *
   * @param message the default message of this validator.
   */
  public NbrOfSharesToRecoverValidator(String message) {
    super(message);

    this.baseMessage = new SimpleStringProperty(this, "baseMessage");
    this.nbrOfSharesText = new SimpleStringProperty(this, "nbrOfSharesText");

    this.baseMessage.addListener((observable, oldValue, newValue) -> formatMessage());
    this.nbrOfSharesText.addListener((observable, oldValue, newValue) -> formatMessage());
  }

  private void formatMessage() {
    if (!Strings.isNullOrEmpty(getBaseMessage())) {
      this.setMessage(MessageFormat.format(getBaseMessage(), getNbrOfSharesText()));
    }
  }

  @Override
  protected void eval() {
    if (srcControl.get() instanceof TextInputControl) {
      String text = ((TextInputControl) srcControl.get()).getText();

      Integer value = parseInteger(text);
      Integer nbrOfShares = parseInteger(getNbrOfSharesText());

      if (value != null && nbrOfShares != null) {
        boolean isSmallerThanNbrOfShares = value <= nbrOfShares;
        hasErrors.set(!isSmallerThanNbrOfShares);
      }

    }
  }

  private Integer parseInteger(String text) {
    Integer result = null;

    try {
      result = Integer.parseInt(text);
    } catch (NumberFormatException e) {
      logger.warn("[{}] is not a number", text, e);
      hasErrors.set(true);
    }

    return result;
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Set the expected maximum number of shares.
   *
   * @param value the expected maximum number of shares.
   */
  public final void setNbrOfSharesText(String value) {
    nbrOfSharesText.set(value);
  }

  /**
   * Get the expected maximum number of shares.
   *
   * @return the expected maximum number of shares.
   */
  public final String getNbrOfSharesText() {
    return nbrOfSharesText.get();
  }

  /**
   * The expected maximum number of shares property.
   *
   * @return the expected maximum number of shares property.
   */
  public final StringProperty nbrOfSharesTextProperty() {
    return nbrOfSharesText;
  }

  /**
   * Set the base message pattern. If this property is set the {@link #messageProperty()} of this validator will be
   * overridden using this pattern, the pattern must accept only one integer which corresponds to the {@link
   * #nbrOfSharesTextProperty()} of this instance.
   *
   * @param value the base message pattern.
   *
   * @see MessageFormat
   */
  public final void setBaseMessage(String value) {
    baseMessage.set(value);
  }

  /**
   * Get the base message pattern.
   *
   * @return the base message pattern.
   */
  public final String getBaseMessage() {
    return baseMessage.get();
  }

  /**
   * The base message pattern property.
   *
   * @return the base message pattern property.
   */
  public final StringProperty baseMessageProperty() {
    return baseMessage;
  }
}
