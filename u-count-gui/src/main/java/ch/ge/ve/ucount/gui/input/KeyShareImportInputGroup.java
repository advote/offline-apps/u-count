/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.input;

import ch.ge.ve.javafx.gui.control.MessageContainer;
import ch.ge.ve.javafx.gui.utils.FXMLLoaderUtils;
import ch.ge.ve.javafx.gui.utils.FileBrowserService;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.nio.file.Path;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

/**
 * A JavaFX element that prompts the user to input the necessary information to recover a {@link
 * ch.ge.ve.ucount.util.key.KeyShare}, i.e.:
 * <ul>
 * <li>The location of the key store file.</li>
 * <li>The passphrase that grants access to the key store.</li>
 * </ul>
 */
public class KeyShareImportInputGroup extends VBox {

  @FXML
  private MessageContainer errorMessageContainer;

  @FXML
  private JFXTextField sourcePathTextField;

  @FXML
  private JFXPasswordField passphrase;

  private final ObjectProperty<FileBrowserService>  fileBrowserService;
  private final ObjectProperty<EncryptionPublicKey> electionOfficerPublicKey;
  private final ObjectProperty<Path>                keySharePath;

  /**
   * Create a new {@link KeyShareImportInputGroup} instance with a default file browser.
   */
  public KeyShareImportInputGroup() {
    this(new FileBrowserService(), null);
  }

  /**
   * Create a new {@link KeyShareImportInputGroup} instance.
   *
   * @param fileBrowserService       the file browser service that will be used to select the source file.
   * @param electionOfficerPublicKey the election officer public key that will be used to verify the integrity of the
   *                                 key share.
   */
  public KeyShareImportInputGroup(FileBrowserService fileBrowserService,
                                  EncryptionPublicKey electionOfficerPublicKey) {
    this.fileBrowserService =
        new SimpleObjectProperty<>(this, "fileBrowserService", fileBrowserService);
    this.electionOfficerPublicKey =
        new SimpleObjectProperty<>(this, "electionOfficerPublicKey", electionOfficerPublicKey);
    this.keySharePath =
        new SimpleObjectProperty<>(this, "keySharePath");

    FXMLLoaderUtils.load(this, "/view/KeyShareImportInputGroup.fxml");
  }

  /**
   * Open the file chooser dialog to select a destination folder for the generated private key.
   */
  @FXML
  public final void selectSourceFile() {
    keySharePath.set(getFileBrowserService().selectFile(sourcePathTextField, "pfx"));
  }


  /**
   * Validates all the input fields in this form, i.e.:
   * <ul>
   * <li>The source path is not empty.</li>
   * <li>The passphrase is at least 20 characters long.</li>
   * </ul>
   *
   * @return true if the value is valid else false
   */
  public final boolean validate() {
    return sourcePathTextField.validate() &&
           passphrase.validate();
  }

  /**
   * Clears all the fields in this input group.
   */
  public final void clear() {
    passphrase.clear();
    sourcePathTextField.clear();
    keySharePath.set(null);
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Get the file browser service used to select the source file.
   *
   * @return the file browser service.
   */
  public final FileBrowserService getFileBrowserService() {
    return fileBrowserService.get();
  }

  /**
   * Set the file browser service that will be used to select the source file.
   *
   * @param value the file browser service.
   */
  public final void setFileBrowserService(FileBrowserService value) {
    fileBrowserService.setValue(value);
  }

  /**
   * The file browser service property.
   *
   * @return the file browser service property.
   */
  public final ObjectProperty<FileBrowserService> fileBrowserServiceProperty() {
    return fileBrowserService;
  }

  /**
   * Get the election officer public key that is used to verify the integrity of the key share.
   *
   * @return the {@link EncryptionPublicKey}.
   */
  public final EncryptionPublicKey getElectionOfficerPublicKey() {
    return electionOfficerPublicKey.get();
  }

  /**
   * Set the election officer public key that will be used to verify the integrity of the key share.
   *
   * @param value the {@link EncryptionPublicKey}.
   */
  public final void setElectionOfficerPublicKey(EncryptionPublicKey value) {
    electionOfficerPublicKey.setValue(value);
  }

  /**
   * The election officer public key property.
   *
   * @return the election officer public key property.
   */
  public final ObjectProperty<EncryptionPublicKey> electionOfficerPublicKeyProperty() {
    return electionOfficerPublicKey;
  }

  /**
   * Get the passphrase set by the user. This function can return an invalid passphrase, call {@link #validate()} before
   * retrieving this value to check for validity.
   *
   * @return the passphrase.
   */
  public final String getPassphrase() {
    return passphrase.textProperty().get();
  }

  /**
   * The text property of the passphrase field.
   *
   * @return the text property of the passphrase field
   */
  public final StringProperty passphraseProperty() {
    return passphrase.textProperty();
  }

  /**
   * Get the path to the key share that the user has set. This function can return an invalid path, all {@link
   * #validate()} before retrieving this value to check for validity.
   *
   * @return the key share path.
   */
  public final Path getKeySharePath() {
    return keySharePath.get();
  }

  /**
   * The key share path property.
   *
   * @return the key share path property.
   */
  public final ObjectProperty<Path> keySharePathProperty() {
    return keySharePath;
  }

  /**
   * Get the error message of this input group's error message container.
   *
   * @return the current error message.
   */
  public final String getErrorMessage() {
    return errorMessageContainer.getMessage();
  }

  /**
   * Set the error message of this input group's error message container.
   *
   * @param value the new error message.
   */
  public final void setErrorMessage(String value) {
    errorMessageContainer.setMessage(value);
  }

  /**
   * This input group's error message container property.
   *
   * @return the error message property.
   */
  public final StringProperty errorMessageProperty() {
    return errorMessageContainer.messageProperty();
  }
}
