/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.conf;

import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer;
import ch.ge.ve.model.convert.api.VoterChoiceConverter;
import ch.ge.ve.model.convert.impl.DefaultVoterChoiceConverter;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import ch.ge.ve.ucount.util.key.KeyPairFactory;
import ch.ge.ve.ucount.util.key.KeyPairFactoryDefaultImpl;
import ch.ge.ve.ucount.util.key.KeyShareCodec;
import ch.ge.ve.ucount.util.key.KeyShareCodecPkcs12Impl;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.math.BigInteger;

/**
 * Configuration class for the U-Count application.
 */
@Configuration
@ComponentScan("ch.ge.ve")
public class UCountConfiguration {

  @Bean
  public RandomGenerator randomGenerator(@Value("${ch.ge.ve.rng.algorithm}") String rngAlgorithm,
                                         @Value("${ch.ge.ve.rng.provider}") String rngProvider) {
    return RandomGenerator.create(rngAlgorithm, rngProvider);
  }

  @Bean
  public PublicParameters publicParameters(@Value("${ucount.security-level}") int securityLevel,
                                           @Value("${ucount.nbrOfControlComponents}") int numberOfControlComponents) {
    PublicParametersFactory publicParametersFactory = PublicParametersFactory.forLevel(securityLevel);
    return publicParametersFactory.createPublicParameters(numberOfControlComponents);
  }

  @Bean
  public KeyShareCodec keyShareCodec() {
    return new KeyShareCodecPkcs12Impl();
  }

  @Bean
  public KeyPairFactory keyPairFactory(RandomGenerator randomGenerator) {
    return new KeyPairFactoryDefaultImpl(randomGenerator);
  }

  @Bean
  public VoterChoiceConverter voterChoiceConverter() {
    return new DefaultVoterChoiceConverter();
  }

  @Bean
  public ObjectMapper objectMapper() {
    SimpleModule module = new SimpleModule();

    module.addSerializer(BigInteger.class, new BigIntegerAsBase64Serializer());
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer());

    ObjectMapper mapper = new ObjectMapper().registerModule(module);
    mapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);

    return mapper;
  }

  @Bean
  @ConfigurationProperties("ucount.executor")
  public TaskExecutor taskExecutor() {
    return new ThreadPoolTaskExecutor();
  }
}
