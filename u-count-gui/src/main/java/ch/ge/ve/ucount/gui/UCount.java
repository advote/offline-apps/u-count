/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui;

import ch.ge.ve.javafx.business.i18n.LanguageUtils;
import ch.ge.ve.javafx.gui.preloader.SplashScreenPreloader;
import ch.ge.ve.javafx.gui.utils.EventHandlers;
import ch.ge.ve.javafx.gui.utils.FXMLLoaderUtils;
import java.io.IOException;
import java.security.Security;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * The U-Count javafx application.
 */
@SpringBootApplication
public class UCount extends Application {

  private static ConfigurableApplicationContext context;

  /**
   * Launch the JavaFX application.
   */
  public static void main(String[] args) {
    System.setProperty(SplashScreenPreloader.BACKGROUND_LOCATION_PROPERTY_KEY, "/images/splash-screen.png");
    System.setProperty("javafx.preloader", SplashScreenPreloader.class.getCanonicalName());

    LoggerFactory.getLogger(UCount.class).info("Launching U-Count...");
    launch(args);
  }

  @Override
  public void init() {
    Security.addProvider(new BouncyCastleProvider());
    SpringApplicationBuilder builder = new SpringApplicationBuilder(UCount.class);
    setContext(builder.run(getParameters().getRaw().toArray(new String[0])));
  }

  @Override
  public void start(Stage stage) throws Exception {
    Parent parent = FXMLLoaderUtils.load(context, "/view/RootLayout.fxml");

    stage.setTitle(LanguageUtils.getCurrentResourceBundle().getString("application.title"));
    stage.setOnCloseRequest(EventHandlers.createExitDialogEventHandler(stage));
    stage.setScene(new Scene(parent, 800, 600));
    stage.getIcons().addAll(
        loadImage("icon_64x64.png"),
        loadImage("icon_32x32.png"),
        loadImage("icon_16x16.png")
    );
    stage.setResizable(true);

    stage.show();
  }

  @Override
  public void stop() {
    context.close();
  }

  /**
   * Statically expose the current spring context so that it can be used on FXML nested elements that are initialized
   * outside of the context.
   *
   * @return the current spring application context.
   *
   * @see ch.ge.ve.ucount.gui.validator.KeyShareValidator
   */
  public static ApplicationContext getContext() {
    return context;
  }

  private static void setContext(ConfigurableApplicationContext context) {
    UCount.context = context;
  }

  private static Image loadImage(String filename) throws IOException {
    ClassPathResource resource = new ClassPathResource("images/" + filename);
    return new Image(resource.getInputStream());
  }
}
