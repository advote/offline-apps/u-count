/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.controller;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableBooleanValue;
import org.springframework.stereotype.Component;

/**
 * Expose the state of the application via JavaFx "observable properties" system.
 * <p>
 * Allow to bind GUI elements to some of the application business-oriented information values.
 * </p>
 *
 * <h3 id="predefined">List of properties</h3>
 *
 * <table class="striped" style="text-align:left">
 * <thead>
 * <tr>
 * <th scope="col">Property</th>
 * <th scope="col">Description</th>
 * <th scope="col">Initial value</th>
 * </tr>
 * </thead>
 * <tbody>
 * <tr>
 * <th scope="row">resultGenerationInProgress</th>
 * <td>Whether a result generation task is in progress</td>
 * <td>false</td>
 * </tr>
 * <tr>
 * <th scope="row">electionOfficerKeyGenerationInProgress</th>
 * <td>Whether an election officer key is being generated.</td>
 * <td>false</td>
 * </tr>
 * <tr>
 * <th scope="row">electionOfficerKeyRecoveryInProgress</th>
 * <td>Whether there is a task recovering an election officer key.</td>
 * <td>false</td>
 * </tr>
 * </tbody>
 * </table>
 */
@Component
public class ApplicationState {

  private final BooleanProperty resultGenerationInProgress;
  private final BooleanProperty electionOfficerKeyGenerationInProgress;
  private final BooleanProperty electionOfficerKeyRecoveryInProgress;

  /**
   * Create a new application state with the initial values.
   */
  public ApplicationState() {
    resultGenerationInProgress =
        new SimpleBooleanProperty(this, "resultGenerationInProgress", false);
    electionOfficerKeyGenerationInProgress =
        new SimpleBooleanProperty(this, "electionOfficerKeyGenerationInProgress", false);
    electionOfficerKeyRecoveryInProgress =
        new SimpleBooleanProperty(this, "electionOfficerKeyRecoveryInProgress", false);
  }

  /**
   * Set whether a result generation task is in progress. Setting this property to true will make the application's
   * {@link #isLoading()} state to be true.
   *
   * @param resultGenerationInProgress whether a result generation task is in progress.
   */
  public void setResultGenerationInProgress(boolean resultGenerationInProgress) {
    this.resultGenerationInProgress.set(resultGenerationInProgress);
  }

  /**
   * Whether a result generation task is in progress.
   *
   * @return a boolean {@link javafx.beans.property.ReadOnlyProperty} that describes whether a result generation task is
   * in progress.
   */
  public ReadOnlyBooleanProperty resultGenerationInProgressProperty() {
    return this.resultGenerationInProgress;
  }

  /**
   * Set whether an election officer key is being generated. Setting this property to true will make the application's
   * {@link #isLoading()} and {@link #isWaiting()} states to be true.
   *
   * @param printerKeyGenerationInProgress whether an election officer key is being generated.
   */
  public void setElectionOfficerKeyGenerationInProgress(boolean printerKeyGenerationInProgress) {
    this.electionOfficerKeyGenerationInProgress.set(printerKeyGenerationInProgress);
  }

  /**
   * Whether an election officer key is being generated.
   *
   * @return a boolean {@link javafx.beans.property.ReadOnlyProperty} that describes whether an election officer key is
   * being generated.
   */
  public ReadOnlyBooleanProperty electionOfficerKeyGenerationInProgressProperty() {
    return electionOfficerKeyGenerationInProgress;
  }

  /**
   * Set whether there is a task recovering an election officer key. Setting this property to true will make the
   * application's {@link #isLoading()} and {@link #isWaiting()} states to be true.
   *
   * @param printerKeyGenerationInProgress whether there is a task recovering an election officer key.
   */
  public void setElectionOfficerKeyRecoveryInProgress(boolean printerKeyGenerationInProgress) {
    this.electionOfficerKeyRecoveryInProgress.set(printerKeyGenerationInProgress);
  }

  /**
   * Whether there is a task recovering an election officer key.
   *
   * @return a boolean {@link javafx.beans.property.ReadOnlyProperty} that describes whether there is a task recovering
   * an election officer key.
   */
  public ReadOnlyBooleanProperty electionOfficerKeyRecoveryInProgressProperty() {
    return electionOfficerKeyRecoveryInProgress;
  }

  /**
   * Whether the application is busy with a background task at the moment. While the value returned by this method is
   * true the submenu and the language picker of the application will be disabled.
   *
   * @return a boolean {@link javafx.beans.value.ObservableValue} that describes whether the application is busy with a
   * background task at the moment
   *
   * @see #setResultGenerationInProgress(boolean)
   * @see #setElectionOfficerKeyGenerationInProgress(boolean)
   * @see #setElectionOfficerKeyRecoveryInProgress(boolean)
   */
  public ObservableBooleanValue isLoading() {
    return resultGenerationInProgress.or(isWaiting());
  }

  /**
   * Whether the application is waiting for a background task to be completed. While the value returned by this method
   * is true a spinner will appear and the content will be disabled.
   *
   * @return a boolean {@link javafx.beans.value.ObservableValue} that describes whether the application is busy with a
   * background task at the moment
   *
   * @see #setElectionOfficerKeyGenerationInProgress(boolean)
   * @see #setElectionOfficerKeyRecoveryInProgress(boolean)
   */
  public ObservableBooleanValue isWaiting() {
    return electionOfficerKeyGenerationInProgress.or(electionOfficerKeyRecoveryInProgress);
  }
}
