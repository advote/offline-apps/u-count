/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.controller;

import ch.ge.ve.filenamer.TallyArchiveFileName;
import ch.ge.ve.javafx.business.json.JsonPathMapper;
import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.javafx.gui.control.MessageContainer;
import ch.ge.ve.javafx.gui.progress.ProgressBarTracker;
import ch.ge.ve.javafx.gui.utils.FileBrowserService;
import ch.ge.ve.javafx.gui.utils.TaskUtils;
import ch.ge.ve.protocol.core.model.EncryptionKeyPair;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.ucount.business.audit.AuditLogWriter;
import ch.ge.ve.ucount.business.context.model.ElectionContext;
import ch.ge.ve.ucount.business.context.model.ElectionContextFactory;
import ch.ge.ve.ucount.business.context.model.TallyArchive;
import ch.ge.ve.ucount.business.context.model.TallyArchiveFactory;
import ch.ge.ve.ucount.business.context.model.TallyContext;
import ch.ge.ve.ucount.business.key.ElectionOfficerKeyService;
import ch.ge.ve.ucount.business.tally.TallyGenerator;
import ch.ge.ve.ucount.business.tally.model.TallyResult;
import ch.ge.ve.ucount.business.xml.ech110.ECH0110Generator;
import ch.ge.ve.ucount.business.xml.ech222.ECH0222Generator;
import ch.ge.ve.ucount.gui.control.ExtendedProgressTracker;
import ch.ge.ve.ucount.gui.control.GroupingProgressTracker;
import ch.ge.ve.ucount.gui.controller.exception.TaskExecutionException;
import ch.ge.ve.ucount.gui.input.ElectionOfficerKeyRecoveryInputGroup;
import com.google.common.base.Stopwatch;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

/**
 * JavaFX Controller for the results file generation view.
 */
@Component
public class GenerateResultFilesController {
  private static final String ERROR_TRANSLATION_KEY = "generate-results-file.exception";

  private static final Logger logger = LoggerFactory.getLogger(GenerateResultFilesController.class);

  private final TallyArchiveFactory             tallyArchiveFactory;
  private final ElectionContextFactory          electionContextFactory;
  private final ElectionOfficerKeyService       electionOfficerKeyService;
  private final ElectionOfficerKeyConfiguration electionOfficerKeyConfiguration;
  private final JsonPathMapper                  jsonPathMapper;
  private final TallyGenerator                  tallyGenerator;
  private final AuditLogWriter                  auditLogWriter;
  private final ECH0110Generator                ech0110Generator;
  private final ECH0222Generator                ech0222Generator;
  private final FileBrowserService              fileBrowserService;
  private final TaskExecutor                    taskExecutor;
  private final ApplicationState                applicationState;

  private CountDownLatch isExtractionReadySignal;
  private CountDownLatch isTallyReadySignal;
  private CountDownLatch areFilesReadySignal;

  private TallyArchive    tallyArchive;
  private ElectionContext electionContext;
  private TallyResult     tallyResult;

  @FXML
  private MessageContainer errorMessageContainer;

  @FXML
  private VBox importBox;

  @FXML
  private HBox actionBox;

  @FXML
  private VBox electionOfficerKeyRecoveryInputBox;

  @FXML
  private VBox progressBox;

  @FXML
  private JFXTextField tallyArchivePath;

  @FXML
  private JFXTextField publicKeyPath;

  @FXML
  private JFXTextField destinationPath;

  @FXML
  private HBox extractTallyArchiveProgressRow;

  @FXML
  private JFXProgressBar extractTallyArchiveProgress;

  @FXML
  private HBox tallyResultGenerationProgressRow;

  @FXML
  private JFXProgressBar tallyResultGenerationProgress;

  @FXML
  private HBox auditLogGenerationProgressRow;

  @FXML
  private JFXProgressBar auditLogGenerationProgress;

  @FXML
  private HBox echGenerationProgressRow;

  @FXML
  private JFXProgressBar echGenerationProgress;

  @Autowired
  @SuppressWarnings("squid:S00107") // Acceptable for a Spring-managed UI-Controller
  public GenerateResultFilesController(TallyArchiveFactory tallyArchiveFactory,
                                       ElectionContextFactory electionContextFactory,
                                       ElectionOfficerKeyService electionOfficerKeyService,
                                       ElectionOfficerKeyConfiguration electionOfficerKeyConfiguration,
                                       JsonPathMapper jsonPathMapper,
                                       TallyGenerator tallyGenerator,
                                       AuditLogWriter auditLogWriter,
                                       ECH0110Generator ech0110Generator,
                                       ECH0222Generator ech0222Generator,
                                       FileBrowserService fileBrowserService,
                                       TaskExecutor taskExecutor,
                                       ApplicationState applicationState) {
    this.tallyArchiveFactory = tallyArchiveFactory;
    this.electionContextFactory = electionContextFactory;
    this.electionOfficerKeyService = electionOfficerKeyService;
    this.electionOfficerKeyConfiguration = electionOfficerKeyConfiguration;
    this.jsonPathMapper = jsonPathMapper;
    this.tallyGenerator = tallyGenerator;
    this.auditLogWriter = auditLogWriter;
    this.ech0110Generator = ech0110Generator;
    this.ech0222Generator = ech0222Generator;
    this.fileBrowserService = fileBrowserService;
    this.taskExecutor = taskExecutor;
    this.applicationState = applicationState;
  }

  /**
   * Open the file chooser dialog to import a tally archive.
   */
  @FXML
  public void selectTallyArchiveFile() {
    fileBrowserService.selectFile(tallyArchivePath, TallyArchiveFileName.EXTENSION);
  }

  /**
   * Open the file chooser dialog to import the election authority's public key.
   */
  @FXML
  public void selectPublicKeyFile() {
    fileBrowserService.selectFile(publicKeyPath, "json");
  }

  /**
   * Open the file chooser dialog to select a destination folder for the generated results file,
   */
  @FXML
  public void selectDestinationFolder() {
    fileBrowserService.selectDirectory(destinationPath);
  }

  /**
   * Submit the tally archive, if the the tally archive is successfully validated the user is prompted to recover the
   * election officer key. Once the key has been successfully recovered the generation of the result file will start
   * automatically.
   *
   * @see ElectionOfficerKeyRecoveryInputGroup
   */
  @FXML
  public void submitTallyArchive() {
    if (tallyArchivePath.validate() &&
        publicKeyPath.validate() &&
        destinationPath.validate()) {

      electionOfficerKeyRecoveryInputBox.getChildren().clear();

      ElectionOfficerKeyRecoveryInputGroup electionOfficerKeyRecoveryInputGroup =
          new ElectionOfficerKeyRecoveryInputGroup(fileBrowserService, applicationState, electionOfficerKeyService,
                                                   electionOfficerKeyConfiguration, taskExecutor);
      electionOfficerKeyRecoveryInputGroup.setElectionOfficerPublicKey(
          jsonPathMapper.map(Paths.get(publicKeyPath.getText()), EncryptionPublicKey.class)
      );
      electionOfficerKeyRecoveryInputGroup.setOnElectionOfficerKeyPairReady(
          event -> startGeneration(electionOfficerKeyRecoveryInputGroup.getElectionOfficerKeyPair())
      );

      electionOfficerKeyRecoveryInputBox.getChildren().add(electionOfficerKeyRecoveryInputGroup);

      importBox.setVisible(false);
      importBox.setManaged(false);
      actionBox.setVisible(false);
      actionBox.setManaged(false);
      electionOfficerKeyRecoveryInputBox.setManaged(true);
      electionOfficerKeyRecoveryInputBox.setVisible(true);
    }
  }

  private void startGeneration(EncryptionKeyPair electionOfficerKeyPair) {
    TaskUtils.clearAllIcons(extractTallyArchiveProgressRow, tallyResultGenerationProgressRow);

    applicationState.setResultGenerationInProgress(true);
    electionOfficerKeyRecoveryInputBox.getChildren().clear();
    electionOfficerKeyRecoveryInputBox.setManaged(false);
    electionOfficerKeyRecoveryInputBox.setVisible(false);
    progressBox.setVisible(true);
    progressBox.setManaged(true);

    isExtractionReadySignal = new CountDownLatch(1);
    isTallyReadySignal = new CountDownLatch(1);
    areFilesReadySignal = new CountDownLatch(3);

    GroupingProgressTracker groupTracker = new GroupingProgressTracker(new ProgressBarTracker(echGenerationProgress));
    final ExtendedProgressTracker ech0222Tracker = groupTracker.createPartialTracker();
    final ExtendedProgressTracker ech0110Tracker = groupTracker.createPartialTracker();
    groupTracker.setOnComplete(nbTasks -> Platform.runLater(
        () -> TaskUtils.onSucceeded(echGenerationProgressRow)
    ));
    groupTracker.setOnFailure(throwable -> Platform.runLater(
        () -> {
          TaskUtils.onFailed(throwable, echGenerationProgressRow);
          echGenerationProgress.setProgress(1);
          errorMessageContainer.setMessage(TaskUtils.formatErrorMessage(ERROR_TRANSLATION_KEY, throwable));
        }
    ));

    extractTallyArchive(electionOfficerKeyPair);
    generateTallyResult();
    writeProtocolAuditLog();
    writeECH0222(ech0222Tracker);
    writeECH0110(ech0110Tracker);
    cleanUp();
  }

  private <T> void onTaskFailed(Task<T> task, Pane progressBarContainer, JFXProgressBar progressBar) {
    TaskUtils.onFailed(task, progressBarContainer);
    errorMessageContainer.setMessage(TaskUtils.formatErrorMessage(ERROR_TRANSLATION_KEY, task));
    Platform.runLater(() -> progressBar.setProgress(1));
  }

  private void extractTallyArchive(EncryptionKeyPair electionOfficerKeyPair) {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() {
        Stopwatch stopwatch = Stopwatch.createStarted();
        ProgressTracker tracker = new ProgressBarTracker(extractTallyArchiveProgress);

        tallyArchive = tallyArchiveFactory.readTallyArchive(Paths.get(tallyArchivePath.getText()));
        tracker.updateProgress(20, 100);

        electionContext = electionContextFactory.createElectionContext(
            tallyArchive.getElectionSet(),
            tallyArchive.getPublicParameters(),
            electionOfficerKeyPair.getPrivateKey(),
            electionOfficerKeyPair.getPublicKey()
        );
        tracker.updateProgress(1, 1);

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Failed to prepare tally archive", event.getSource().getException());
      onExtractTallyArchiveFailed(task);
    });

    task.setOnSucceeded(event -> {
      try {
        TaskUtils.onSucceeded(extractTallyArchiveProgressRow);
        logger.info("Tally archive preparation took [{}] seconds", task.get().getSeconds());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the tally archive preparation task", e);
        onExtractTallyArchiveFailed(task);
        throw new TaskExecutionException("Cannot retrieve the result of the tally archive preparation task", e);
      } catch (InterruptedException e) {
        logger.error("The tally archive preparation task was interrupted", e);
        onExtractTallyArchiveFailed(task);
        Thread.currentThread().interrupt();
      } finally {
        isExtractionReadySignal.countDown();
      }
    });

    taskExecutor.execute(task);
  }

  private <T> void onExtractTallyArchiveFailed(Task<T> task) {
    onTaskFailed(task, extractTallyArchiveProgressRow, extractTallyArchiveProgress);
    isExtractionReadySignal.countDown();
  }

  private void generateTallyResult() {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() throws Exception {
        awaitTallyArchiveExtraction();

        Stopwatch stopwatch = Stopwatch.createStarted();

        TallyContext tallyContext = TallyContext.from(tallyArchive);

        tallyResult = tallyGenerator.generateTally(electionContext,
                                                   tallyContext.getPrimes(),
                                                   tallyContext.getFinalShuffle(),
                                                   tallyContext.getControlComponentsPartialDecryptions(),
                                                   tallyContext.getCountingCircles(),
                                                   new ProgressBarTracker(tallyResultGenerationProgress));

        Platform.runLater(() -> tallyResultGenerationProgress.setProgress(1));

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Failed to generate the tally result", event.getSource().getException());
      onGenerateTallyResultFailed(task);
    });

    task.setOnSucceeded(event -> {
      try {
        TaskUtils.onSucceeded(tallyResultGenerationProgressRow);
        logger.info("The tallying task took [{}] seconds", task.get().getSeconds());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the tallying task", e);
        onGenerateTallyResultFailed(task);
        throw new TaskExecutionException("Cannot retrieve the result of the tallying task", e);
      } catch (InterruptedException e) {
        logger.error("The tallying task was interrupted", e);
        onGenerateTallyResultFailed(task);
        Thread.currentThread().interrupt();
      } finally {
        isTallyReadySignal.countDown();
      }
    });

    taskExecutor.execute(task);
  }

  private <T> void onGenerateTallyResultFailed(Task<T> task) {
    onTaskFailed(task, tallyResultGenerationProgressRow, tallyResultGenerationProgress);
    isTallyReadySignal.countDown();
  }

  private void awaitTallyArchiveExtraction() throws InterruptedException {
    isExtractionReadySignal.await();

    if (tallyArchive == null) {
      throw new IllegalStateException("Tally archive extraction failed");
    }
  }

  private void writeProtocolAuditLog() {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() throws Exception {
        awaitTallyArchiveExtraction();
        isTallyReadySignal.await();
        Stopwatch stopwatch = Stopwatch.createStarted();
        ProgressTracker tracker = new ProgressBarTracker(auditLogGenerationProgress);

        Path destination = Paths.get(destinationPath.getText());
        auditLogWriter.writeProtocolAuditLog(electionContext, tallyArchive, tallyResult, destination, tracker);

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Failed to generate the audit log", event.getSource().getException());
      onAuditLogGenerationFailed(task);
    });

    task.setOnSucceeded(event -> {
      try {
        TaskUtils.onSucceeded(auditLogGenerationProgressRow);
        logger.info("The audit log generation took [{}] seconds", task.get().getSeconds());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the audit log generation task", e);
        onAuditLogGenerationFailed(task);
        throw new TaskExecutionException("Cannot retrieve the result of the audit log generation task", e);
      } catch (InterruptedException e) {
        logger.error("The audit log generation task was interrupted", e);
        onAuditLogGenerationFailed(task);
        Thread.currentThread().interrupt();
      } finally {
        areFilesReadySignal.countDown();
      }
    });

    taskExecutor.execute(task);
  }

  private <T> void onAuditLogGenerationFailed(Task<T> task) {
    onTaskFailed(task, auditLogGenerationProgressRow, auditLogGenerationProgress);
    areFilesReadySignal.countDown();
  }

  private void writeECH0222(ExtendedProgressTracker progressTracker) {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() throws Exception {
        awaitTallyArchiveExtraction();
        isTallyReadySignal.await();
        Stopwatch stopwatch = Stopwatch.createStarted();

        Path destination = Paths.get(destinationPath.getText());
        ech0222Generator.generateECH0222(tallyArchive, tallyResult, destination, progressTracker);

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Failed to generated eCH-0222", event.getSource().getException());
      onECHGenerationFailed(progressTracker, event.getSource().getException());
    });

    task.setOnSucceeded(event -> {
      try {
        progressTracker.updateProgress(1, 1);
        logger.info("The eCH-0222 generation took [{}] seconds", task.get().getSeconds());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the eCH-0222 generation task", e);
        onECHGenerationFailed(progressTracker, e);
        throw new TaskExecutionException("Cannot retrieve the result of the eCH-0222 generation task", e);
      } catch (InterruptedException e) {
        logger.error("The eCH-0222 generation task has been interrupted", e);
        onECHGenerationFailed(progressTracker, e);
        Thread.currentThread().interrupt();
      } finally {
        areFilesReadySignal.countDown();
      }
    });

    taskExecutor.execute(task);
  }

  private void onECHGenerationFailed(ExtendedProgressTracker progressTracker, Throwable cause) {
    progressTracker.notifyFailure(cause);
    areFilesReadySignal.countDown();
  }


  private void writeECH0110(ExtendedProgressTracker progressTracker) {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() throws Exception {
        logger.debug("Waiting for tallyArchive to be extracted...");
        awaitTallyArchiveExtraction();
        logger.info("Reading TallyArchive for ech110 preparation.");
        final Stopwatch stopwatch = Stopwatch.createStarted();
        final ECH0110Generator.PreparedGenerator preparedGenerator = ech0110Generator.prepare(tallyArchive);
        progressTracker.updateProgress(33, 100);

        logger.debug("Waiting for tally result to be prepared...");
        isTallyReadySignal.await();
        logger.info("Starting ech110 generation.");

        preparedGenerator.write(tallyResult, Paths.get(destinationPath.getText()));
        progressTracker.updateProgress(95, 100);

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Failed to generated eCH-0110", event.getSource().getException());
      onECHGenerationFailed(progressTracker, event.getSource().getException());
    });

    task.setOnSucceeded(event -> {
      try {
        progressTracker.updateProgress(100, 100);
        logger.info("Ech110 generation finished in [{}] seconds : {}", task.get().getSeconds(),
                    event.getSource().getMessage());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the eCH-0110 generation task", e);
        onECHGenerationFailed(progressTracker, e);
        throw new TaskExecutionException("Cannot retrieve the result of the eCH-0110 generation task", e);
      } catch (InterruptedException e) {
        logger.error("The eCH-0110 generation task has been interrupted", e);
        onECHGenerationFailed(progressTracker, e);
        Thread.currentThread().interrupt();
      } finally {
        areFilesReadySignal.countDown();
      }
    });

    taskExecutor.execute(task);
  }

  private void cleanUp() {
    taskExecutor.execute(() -> {
      try {
        areFilesReadySignal.await();
      } catch (InterruptedException e) {
        logger.warn("The clean up task was unexpectedly interrupted", e);
        Thread.currentThread().interrupt();
      }

      tallyArchive = null;
      tallyResult = null;
      electionContext = null;

      applicationState.setResultGenerationInProgress(false);
    });
  }
}
