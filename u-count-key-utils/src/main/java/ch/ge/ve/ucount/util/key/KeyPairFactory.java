/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key;

import ch.ge.ve.protocol.core.model.EncryptionKeyPair;
import ch.ge.ve.protocol.model.EncryptionGroup;

/**
 * Generates encryption key pairs.
 */
public interface KeyPairFactory {

  /**
   * Generates a split key pair (that is a key pair where the private key has been split into multiple shares according
   * to the given scheme). Note that if the given scheme's {@code k} is equal to {@code 1} then you will get {@code n}
   * copies of the whole private key.
   *
   * @param scheme          the private key sharing scheme.
   * @param encryptionGroup the encryption group to use.
   *
   * @return the generated split key pair.
   *
   * @throws NullPointerException if one of the given arguments is {@code null}.
   */
  SplitKeyPair generateSplitKeyPair(KeySharingScheme scheme, EncryptionGroup encryptionGroup);

  /**
   * Recovers the original encryption key pair from the given split key pair.
   *
   * @param scheme       the private key sharing scheme.
   * @param splitKeyPair the split key pair.
   *
   * @return the recovered key pair.
   *
   * @throws NullPointerException     if one of the given arguments is {@code null}.
   * @throws IllegalArgumentException if there are not enough distinct key shares to recover the original key, or if the
   *                                  recovered private key does not match the expected public key.
   */
  EncryptionKeyPair recoverKeyPair(KeySharingScheme scheme, SplitKeyPair splitKeyPair);
}
