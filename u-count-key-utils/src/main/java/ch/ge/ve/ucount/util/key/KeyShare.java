/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;

/**
 * Model class that represents a private key share.
 */
public final class KeyShare {

  private final int    index;
  private final byte[] value;
  private final byte[] publicKey;

  /**
   * Creates a new key value.
   *
   * @param index     the index of the value.
   * @param value     the key part itself.
   * @param publicKey the public key corresponding to the split private key.
   *
   * @throws NullPointerException     if one of the given arguments is {@code null}.
   * @throws IllegalArgumentException if {@code 256 >= index < 1} or if one of {@code value} or {@code publicKey}
   *                                  is empty.
   */
  KeyShare(int index, byte[] value, byte[] publicKey) {
    Preconditions.checkArgument(value.length > 0, "'value' must not be empty");
    Preconditions.checkArgument(publicKey.length > 0, "'publicKey' must not be empty");
    Preconditions.checkArgument(index > 0 && index <= 256, "'index' must be > 0 and <= 256");
    this.index = index;
    this.value = Arrays.copyOf(value, value.length);
    this.publicKey = Arrays.copyOf(publicKey, publicKey.length);
  }

  /**
   * Returns the part index.
   *
   * @return the part index.
   */
  public int index() {
    return index;
  }

  /**
   * Returns the private key part value.
   *
   * @return the private key part value.
   */
  byte[] value() {
    return Arrays.copyOf(value, value.length);
  }

  /**
   * Returns the public key corresponding to the split private key.
   *
   * @return the public key corresponding to the split private key.
   */
  public byte[] publicKey() {
    return Arrays.copyOf(publicKey, publicKey.length);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KeyShare share = (KeyShare) o;
    return index == share.index && Arrays.equals(publicKey, share.publicKey);
  }

  @Override
  public int hashCode() {
    int hash = Objects.hash(index);
    hash = 31 * hash + Arrays.hashCode(publicKey);
    return hash;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
                      .add("index", index)
                      .add("publicKey", Base64.getEncoder().encode(publicKey))
                      .toString();
  }
}
