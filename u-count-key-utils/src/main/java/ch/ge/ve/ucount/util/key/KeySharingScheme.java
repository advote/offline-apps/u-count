/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import java.util.Objects;

/**
 * Represents a key sharing scheme. A scheme is defined by the total number of shares ({@code n}) and the required
 * number of shares to recover the original key ({@code k}).
 */
public final class KeySharingScheme {

  /**
   * Creates a new {@code KeySharingScheme}.
   *
   * @param k the required number of shares to recover the original key.
   * @param n the total number of shares.
   *
   * @return the created scheme.
   *
   * @throws IllegalArgumentException if {@code n < 1} or if {@code k > n}.
   */
  public static KeySharingScheme of(int k, int n) {
    Preconditions.checkArgument(n >= 1, "n (total number of shares) must be >= 1");
    Preconditions.checkArgument(k > 0 && k <= n,
                                "k (number of required shares) must be > 0 and <= n (total number of shares)");
    Preconditions.checkArgument(n <= 255, "n (total number of shares) must be <= 255");
    return new KeySharingScheme(k, n);
  }

  private final int k;
  private final int n;

  private KeySharingScheme(int k, int n) {
    this.k = k;
    this.n = n;
  }

  /**
   * Returns the required number of shares to recover the original key.
   *
   * @return the required number of shares to recover the original key.
   */
  public int k() {
    return k;
  }

  /**
   * Returns the total number of shares.
   *
   * @return the total number of shares.
   */
  public int n() {
    return n;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KeySharingScheme scheme = (KeySharingScheme) o;
    return k == scheme.k && n == scheme.n;
  }

  @Override
  public int hashCode() {
    return Objects.hash(k, n);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this).add("k", k).add("n", n).toString();
  }
}
