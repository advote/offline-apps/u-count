/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key;

import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.arithmetic.BigIntegerArithmetic;
import ch.ge.ve.protocol.core.model.EncryptionKeyPair;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.EncryptionGroup;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import com.codahale.shamir.Scheme;
import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default {@code KeyPairFactory}'s implementation. The secret sharing mechanism is based on Shamir's Secret Sharing
 * algorithm over GF(256).
 */
public final class KeyPairFactoryDefaultImpl implements KeyPairFactory {

  private static final Logger logger = LoggerFactory.getLogger(KeyPairFactoryDefaultImpl.class);

  private final KeyEstablishmentAlgorithms algorithm;

  /**
   * Creates a new {@code KeyPairFactoryDefaultImpl}.
   *
   * @param random the random generator.
   *
   * @throws NullPointerException if {@code random} is {@code null}.
   */
  public KeyPairFactoryDefaultImpl(RandomGenerator random) {
    Preconditions.checkNotNull(random, "'random' must not be null");
    this.algorithm = new KeyEstablishmentAlgorithms(random);
  }

  @Override
  public SplitKeyPair generateSplitKeyPair(KeySharingScheme scheme, EncryptionGroup encryptionGroup) {
    logger.info("Generating a new key pair...");
    EncryptionKeyPair keyPair = algorithm.generateKeyPair(encryptionGroup);
    logger.info("Splitting private key using a {}/{} scheme", scheme.k(), scheme.n());
    return new SplitKeyPair(keyPair.getPublicKey(), split(scheme, keyPair));
  }

  @Override
  public EncryptionKeyPair recoverKeyPair(KeySharingScheme scheme, SplitKeyPair splitKeyPair) {
    Set<KeyShare> shares = splitKeyPair.getPrivateKeyShares();
    logger.info("Recovering private key from {} shares (using a {}/{} scheme)", shares.size(), scheme.k(), scheme.n());
    Preconditions.checkArgument(shares.size() >= scheme.k(), "Not enough shares to recover the private key");
    EncryptionPublicKey publicKey = splitKeyPair.getPublicKey();
    EncryptionGroup eg = publicKey.getCyclicGroup();
    BigInteger sk = join(scheme, shares);
    Preconditions.checkArgument(BigIntegerArithmetic.modExp(eg.getG(), sk, eg.getP()).equals(publicKey.getPublicKey()),
                                "The recovered private key does not match the given public key");
    return new EncryptionKeyPair(publicKey, new EncryptionPrivateKey(sk, eg));
  }

  private Set<KeyShare> split(KeySharingScheme scheme, EncryptionKeyPair keyPair) {
    byte[] privateKey = keyPair.getPrivateKey().getPrivateKey().toByteArray();
    byte[] publicKey = keyPair.getPublicKey().getPublicKey().toByteArray();
    if (scheme.k() == 1) {
      return IntStream.rangeClosed(1, scheme.n())
                      .mapToObj(i -> new KeyShare(i, privateKey, publicKey))
                      .collect(Collectors.toSet());
    }
    return toScheme(scheme).split(privateKey).entrySet().stream()
                           .map(e -> new KeyShare(e.getKey(), e.getValue(), publicKey))
                           .collect(Collectors.toSet());
  }

  private BigInteger join(KeySharingScheme scheme, Set<KeyShare> shares) {
    Map<Integer, byte[]> parts = shares.stream().collect(Collectors.toMap(KeyShare::index, KeyShare::value));
    if (scheme.k() == 1) {
      Set<BigInteger> values = shares.stream().map(s -> new BigInteger(s.value())).collect(Collectors.toSet());
      Preconditions.checkArgument(values.size() == 1);
      return values.iterator().next();
    }
    return new BigInteger(toScheme(scheme).join(parts));
  }

  private Scheme toScheme(KeySharingScheme scheme) {
    return Scheme.of(scheme.n(), scheme.k());
  }
}
