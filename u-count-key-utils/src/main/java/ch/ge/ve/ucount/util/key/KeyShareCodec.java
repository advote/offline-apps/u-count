/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key;

import java.nio.file.Path;

/**
 * Encodes/decodes key shares into/from password protected bundles.
 */
public interface KeyShareCodec {

  /**
   * Reads a key share from the specified file.
   *
   * @param source   the location of the file to read the key share from.
   * @param password the password protecting the key share.
   *
   * @return the read key share.
   *
   * @throws NullPointerException                    if one of the given arguments is {@code null}.
   * @throws IllegalArgumentException                if the given bundle is deemed invalid.
   * @throws KeyShareDeserializationRuntimeException if an error occurs during the process.
   */
  KeyShare read(Path source, char[] password);

  /**
   * Writes the given key share to the specified file.
   *
   * @param share       the key share to write.
   * @param destination the location of the file to write the key share to.
   * @param password    the password to use to protect the share.
   *
   * @throws NullPointerException                  if one of the given arguments is {@code null}.
   * @throws KeyShareSerializationRuntimeException if an error occurs during the process.
   */
  void write(KeyShare share, Path destination, char[] password);
}
