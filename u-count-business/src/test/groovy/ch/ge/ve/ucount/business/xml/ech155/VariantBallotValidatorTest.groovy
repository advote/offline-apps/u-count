/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech155

import ch.ge.ve.interfaces.ech.eCH0155.v4.QuestionInformationType
import ch.ge.ve.interfaces.ech.eCH0155.v4.TieBreakInformationType
import ch.ge.ve.ucount.business.xml.exception.InconsistentDataException
import spock.lang.Specification

class VariantBallotValidatorTest extends Specification {

  static generateQuestions(int number) {
    (1..number).collect { new QuestionInformationType(questionIdentification: "question_$it") }
  }


  def "validation should pass for the typical expected case"() {
    given: '2 questions and a tiebreak to decide between them'
    def questions = generateQuestions(2)
    def tiebreaks = [
            new TieBreakInformationType(
                    questionIdentification: "tb_1", referencedQuestion1: "question_1", referencedQuestion2: "question_2")
    ]

    when:
    VariantBallotValidator.validateData(questions, tiebreaks)

    then:
    noExceptionThrown()
  }


  def "validation should fail if the reference to question 1 is missing"() {
    given:
    def questions = generateQuestions(2)
    def tiebreaks = [new TieBreakInformationType(questionIdentification: "tb_1")]

    when:
    VariantBallotValidator.validateData(questions, tiebreaks)

    then:
    def ex = thrown NullPointerException
    ex.message == 'TieBreak "tb_1" : missing reference to question 1'
  }


  def "validation should fail if the reference to question 2 is missing"() {
    given:
    def questions = generateQuestions(2)
    def tiebreaks = [
            new TieBreakInformationType(questionIdentification: "tb_1", referencedQuestion1: "question_1")
    ]

    when:
    VariantBallotValidator.validateData(questions, tiebreaks)

    then:
    def ex = thrown NullPointerException
    ex.message == 'TieBreak "tb_1" : missing reference to question 2'
  }


  def "validation should fail if the reference to question 1 does not match a question"() {
    given:
    def questions = generateQuestions(2)
    def tiebreaks = [
            new TieBreakInformationType(
                    questionIdentification: "tb_1", referencedQuestion1: "Q_1", referencedQuestion2: "question_2")
    ]

    when:
    VariantBallotValidator.validateData(questions, tiebreaks)

    then:
    def ex = thrown InconsistentDataException
    ex.message == 'Tie-break "tb_1" references, as question n°1, the id "Q_1" - not found in this ballot : [question_1, question_2]'
  }


  def "validation should pass if there are no tiebreaks"() {
    given:
    def questions = generateQuestions(3)

    when:
    VariantBallotValidator.validateData(questions, [])

    then:
    noExceptionThrown()
  }


  def "validation should accept multiple tiebreaks when all references are consistent"() {
    given:
    def questions = generateQuestions(3)
    def tiebreaks = [
            new TieBreakInformationType(
                    questionIdentification: "tb_1", referencedQuestion1: "question_1", referencedQuestion2: "question_2"),
            new TieBreakInformationType(
                    questionIdentification: "tb_2", referencedQuestion1: "question_1", referencedQuestion2: "question_3"),
            new TieBreakInformationType(
                    questionIdentification: "tb_3", referencedQuestion1: "question_2", referencedQuestion2: "question_3"),
    ]

    when:
    VariantBallotValidator.validateData(questions, tiebreaks)

    then:
    noExceptionThrown()
  }
}
