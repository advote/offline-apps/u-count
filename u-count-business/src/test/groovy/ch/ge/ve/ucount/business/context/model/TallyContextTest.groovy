/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.context.model

import ch.ge.ve.protocol.model.CountingCircle
import ch.ge.ve.protocol.model.Decryptions
import ch.ge.ve.protocol.model.Encryption
import spock.lang.Specification

class TallyContextTest extends Specification {

  def testCountingCircles = [
          new CountingCircle(1, "cc_#1", "First circle"),
          new CountingCircle(2, "cc_#2", "Second circle"),
          new CountingCircle(3, "cc_#3", "Third circle"),
  ]

  def "should be able to compute its data from a TallyArchive"() {
    given:
    def primesList = [1, 5, 11] as List<BigInteger>
    def decryptionsMap = [
            1: new Decryptions([11, 21, 31] as List<BigInteger>),
            0: new Decryptions([10, 20, 30] as List<BigInteger>),
            3: new Decryptions([13, 23, 33] as List<BigInteger>),
            2: new Decryptions([12, 22, 32] as List<BigInteger>)
    ]
    def shufflesMap = [
            1: [new Encryption(1, 11), new Encryption(21, 31)],
            3: [new Encryption(3, 13), new Encryption(23, 33)],
            5: [new Encryption(5, 15), new Encryption(25, 35)],
            2: [new Encryption(2, 12), new Encryption(22, 32)]
    ]
    def numberOfVotersList = [
            new VotersPerCountingCircle(testCountingCircles[0], [a: 1L, b:2L]),
            new VotersPerCountingCircle(testCountingCircles[1], [a: 15L]),
            new VotersPerCountingCircle(testCountingCircles[2], [b: 22L])
    ]

    and:
    def tallyArchive = Mock(TallyArchive) {
      getPrimes() >> primesList
      getPartialDecryptions() >> decryptionsMap
      getShuffles() >> shufflesMap
      getCountingCircles() >> numberOfVotersList
    }

    when:
    def context = TallyContext.from(tallyArchive)

    then:
    context.primes == primesList
    context.controlComponentsPartialDecryptions == [
            decryptionsMap.get(0), decryptionsMap.get(1), decryptionsMap.get(2), decryptionsMap.get(3)
    ]
    context.finalShuffle == shufflesMap.get(5)
    context.getCountingCircles() == testCountingCircles
  }
}
