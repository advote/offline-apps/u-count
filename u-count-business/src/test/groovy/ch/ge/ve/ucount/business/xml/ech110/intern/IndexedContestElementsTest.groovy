/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern

import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotType
import ch.ge.ve.interfaces.ech.eCH0155.v4.QuestionInformationType
import ch.ge.ve.interfaces.ech.eCH0155.v4.TieBreakInformationType
import ch.ge.ve.interfaces.ech.eCH0155.v4.VoteType
import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery
import spock.lang.Specification

class IndexedContestElementsTest extends Specification {

  def "A list of VoteInformation objects should be indexed correctly for id-based reference"() {
    given:
    def rawInformation = [
            new EventInitialDelivery.VoteInformation(vote: new VoteType(voteIdentification: "Vote #1"),
                    ballot: [
                            new BallotType(ballotIdentification: "Ballot #1"),
                            new BallotType(ballotIdentification: "Ballot #2")
                    ]),
            new EventInitialDelivery.VoteInformation(vote: new VoteType(voteIdentification: "Vote #2"),
                    ballot: [
                            new BallotType(ballotIdentification: "Ballot #1"),
                            new BallotType(ballotIdentification: "Ballot #2")
                    ])
    ]

    when:
    def index = IndexedContestElements.from(rawInformation)

    then: "VoteInformation should be retrieved by identification key"
    index.getVoteInformation("Vote #1") == rawInformation[0]
    index.getVoteInformation("Vote #2") == rawInformation[1]

    and: "Ballots should be retrieved by voteId + ballotId"
    index.getBallot("Vote #1", "Ballot #1") == rawInformation[0].ballot[0]
    index.getBallot("Vote #1", "Ballot #2") == rawInformation[0].ballot[1]
    index.getBallot("Vote #2", "Ballot #1") == rawInformation[1].ballot[0]
    index.getBallot("Vote #2", "Ballot #2") == rawInformation[1].ballot[1]
  }

  def "questions and tiebreaks from variant ballots should be indexed and made available"() {
    given:
    def rawInformation = [
            new EventInitialDelivery.VoteInformation(vote: new VoteType(voteIdentification: "Vote #1"),
                    ballot: [
                            new BallotType(ballotIdentification: "Ballot #1", variantBallot: new BallotType.VariantBallot(
                                    questionInformation: [
                                            new QuestionInformationType(questionIdentification: "Q1"),
                                            new QuestionInformationType(questionIdentification: "Q2"),
                                            new QuestionInformationType(questionIdentification: "Q3")
                                    ]
                            )),
                            new BallotType(ballotIdentification: "Ballot #2", variantBallot: new BallotType.VariantBallot(
                                    questionInformation: [
                                            new QuestionInformationType(questionIdentification: "Q1"),
                                            new QuestionInformationType(questionIdentification: "Q2")
                                    ],
                                    tieBreakInformation: [
                                            new TieBreakInformationType(
                                                    questionIdentification: "TB",
                                                    referencedQuestion1: "Q1",
                                                    referencedQuestion2: "Q2"
                                            )
                                    ]
                            ))
                    ]),
            new EventInitialDelivery.VoteInformation(vote: new VoteType(voteIdentification: "Vote #2"),
                    ballot: [
                            new BallotType(ballotIdentification: "Ballot #1"),
                            new BallotType(ballotIdentification: "Ballot #2", variantBallot: new BallotType.VariantBallot(
                                    questionInformation: [
                                            new QuestionInformationType(questionIdentification: "Q1"),
                                            new QuestionInformationType(questionIdentification: "Q2")
                                    ]
                            ))
                    ])
    ]

    when:
    def index = IndexedContestElements.from(rawInformation)

    then:
    index.getQuestionOrTieBreak("Vote #1", "Ballot #1", "Q1").isLeft()
    index.getQuestionOrTieBreak("Vote #1", "Ballot #2", "Q1").isLeft()
    index.getQuestionOrTieBreak("Vote #1", "Ballot #2", "TB").with {
      assert it.isRight()
      it.right.get().referencedQuestion1 == "Q1"
    }
  }

  // A sample with all "requestable" objects
  static createCompleteSample() {
    return [
            new EventInitialDelivery.VoteInformation(vote: new VoteType(voteIdentification: "Vote #1"),
                    ballot: [
                            new BallotType(ballotIdentification: "Ballot #1", standardBallot: new BallotType.StandardBallot()),
                            new BallotType(ballotIdentification: "Ballot #2", variantBallot: new BallotType.VariantBallot(
                                    questionInformation: [
                                            new QuestionInformationType(questionIdentification: "Q1"),
                                            new QuestionInformationType(questionIdentification: "Q2")
                                    ],
                                    tieBreakInformation: [
                                            new TieBreakInformationType(
                                                    questionIdentification: "TB",
                                                    referencedQuestion1: "Q1",
                                                    referencedQuestion2: "Q2"
                                            )
                                    ]
                            ))
                    ])
    ]
  }

  def "index should be strict : request for non-existing VoteInformation should throw exception"() {
    given:
    def index = IndexedContestElements.from(createCompleteSample())

    when:
    index.getVoteInformation("UnexistingId")

    then:
    def ex = thrown(NullPointerException)
    ex.message == 'No VoteInformation indexed with id = "UnexistingId"'
  }

  def "index should be strict : request for non-existing Ballot should throw exception"() {
    given:
    def index = IndexedContestElements.from(createCompleteSample())

    when:
    index.getBallot("Vote #1", "UnexistingBallot")

    then:
    def ex = thrown(NullPointerException)
    ex.message == 'No Ballot indexed with voteId = "Vote #1", ballotId = "UnexistingBallot"'
  }

  def "index should be strict : request for non-existing QuestionOrTieBreak should throw exception"() {
    given:
    def index = IndexedContestElements.from(createCompleteSample())

    when:
    index.getQuestionOrTieBreak("Vote #1", "Ballot #2", "UnexistingQuestion")

    then:
    def ex = thrown(NullPointerException)
    ex.message == 'At least one of the options should be defined - found both null'
  }

}
