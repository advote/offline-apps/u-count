/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110

import static ch.ge.ve.ucount.business.TestUtils.TALLY_ARCHIVE_FOLDER

import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.ucount.business.TestUtils
import ch.ge.ve.ucount.business.xml.XmlTestsConfiguration
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ContextConfiguration(classes = XmlTestsConfiguration)
class ECH0110GeneratorTest extends Specification {

  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder()

  @Autowired
  ECH0110Generator generator

  def "Generator should be able to generate a valid ECH-0110"() {
    given:
    def tracker = Mock(ProgressTracker)
    def archivePath = TALLY_ARCHIVE_FOLDER.resolve("tally-archive_valid-standard-ballot.taf")
    def tallyArchive = TestUtils.createTallyArchive(archivePath)
    def tallyResult = TestUtils.createTallyResult(
            tallyArchive, TestUtils.createElectionContext(tallyArchive), tracker
    )

    when:
    def destinationPath = generator.prepare(tallyArchive)
            .write(tallyResult, temporaryFolder.newFolder().toPath())

    then:
    destinationPath.text.startsWith('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>')

    def delivery = new XmlSlurper().parse(destinationPath.toFile())
    delivery.resultDelivery.contestInformation.contestIdentification == "205706VP"
    delivery.resultDelivery.contestInformation.contestDate == "2057-06-12"
    delivery.resultDelivery.countingCircleResults[0].countingCircle.countingCircleId == "100002"
    delivery.resultDelivery.countingCircleResults[0].voteResults.size() == 5
  }

}
