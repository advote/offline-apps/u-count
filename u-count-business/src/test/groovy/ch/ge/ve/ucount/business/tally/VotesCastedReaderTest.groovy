/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.tally

import static ch.ge.ve.model.convert.model.AnswerTypeEnum.INITIATIVE_COUNTER_PROJECT_BLANK
import static ch.ge.ve.model.convert.model.AnswerTypeEnum.YES_NO
import static ch.ge.ve.model.convert.model.AnswerTypeEnum.YES_NO_BLANK

import ch.ge.ve.ucount.business.xml.VoterChoicesHelper
import com.google.common.base.VerifyException
import spock.lang.Specification

class VotesCastedReaderTest extends Specification {

  def choices = VoterChoicesHelper.forVotationStandardBallot("vote_1", "ballot_1", YES_NO_BLANK) +
          VoterChoicesHelper.forVotationVariantBallot("vote_1", "ballot_2", "question_a", YES_NO) +
          VoterChoicesHelper.forVotationVariantBallot("vote_1", "ballot_2", "question_b", YES_NO) +
          VoterChoicesHelper.forVotationVariantBallot("vote_1", "ballot_2", "tiebreak", INITIATIVE_COUNTER_PROJECT_BLANK)

  def reader = new VotesCastedReader(choices)


  def "BulletinReader should fail if an unexisting question is requested"() {
    when:
    reader.forQuestion("vote_1", "ballot_2", "question_??")

    then:
    def ex = thrown IllegalArgumentException
    ex.message == 'No voter choice matches voteId="vote_1", ballotId="ballot_2", questionId="question_??"'
  }

  def "Request should read the correct answer's index"() {
    given: 'a request for the first question'
    def request = reader.forQuestion("vote_1", "ballot_1", "ballot_1")

    and: 'three different selections'
    def x = true, _ = false
    def selection_1 = [x, _, _] /* we don't care about the other choices */ + [_] * 7
    def selection_2 = [_, x, _] + [_] * 7
    def selection_3 = [_, _, x] + [_] * 7

    expect:
    request.chosenIndex(selection_1) == 0
    request.chosenIndex(selection_2) == 1
    request.chosenIndex(selection_3) == 2
  }

  def "Request should read the correct answer value"() {
    given: 'a request for the first question'
    def request = reader.forQuestion("vote_1", "ballot_1", "ballot_1")

    and: 'three different selections'
    def x = true, _ = false
    def selection_1 = [x, _, _] /* we don't care about the other choices */ + [_] * 7
    def selection_2 = [_, x, _] + [_] * 7
    def selection_3 = [_, _, x] + [_] * 7

    expect:
    request.chosenValue(selection_1).get() == "YES"
    request.chosenValue(selection_2).get() == "NO"
    request.chosenValue(selection_3).get() == "BLANK"
  }

  def "Request should fail if the selection does not have the correct number of answers"() {
    given: 'a request for the tiebreak'
    def request = reader.forQuestion("vote_1", "ballot_2", "tiebreak")

    and: 'a selection with not enough values'
    def selection = [false] * 9

    when:
    request.chosenIndex(selection)

    then:
    def ex = thrown VerifyException
    ex.message == 'A valid selection vector should contain 10 values, but this one has 9'
  }

  def "Request index should return -1 when no answer has been provided (non eligible voter)"() {
    given:
    def request = reader.forQuestion("vote_1", "ballot_2", "tiebreak")
    def selection = [false] * 10

    expect:
    request.chosenIndex(selection) == -1
  }

  def "Request value should be empty when no answer has been provided (non eligible voter)"() {
    given:
    def request = reader.forQuestion("vote_1", "ballot_2", "tiebreak")
    def selection = [false] * 10

    expect:
    !request.chosenValue(selection).isPresent()
  }

  def "Request.isBlank should return whether or not the selection contains a BLANK answer"() {
    given:
    def request_ballot_1 = reader.forQuestion("vote_1", "ballot_1", "ballot_1")
    def request_question_a = reader.forQuestion("vote_1", "ballot_2", "question_a")
    def request_question_b = reader.forQuestion("vote_1", "ballot_2", "question_b")
    def request_tiebreak = reader.forQuestion("vote_1", "ballot_2", "tiebreak")

    def x = true, _ = false
    def selections = [
            [x, _, _] + [x, _] + [_, x] + [x, _, _],
            [_, _, x] + [_, x] + [x, _] + [x, _, _],
            [_, x, _] + [_, x] + [x, _] + [_, _, x]
    ]

    expect:
    selections.collect { request_ballot_1.isBlank(it) } == [false, true, false]
    selections.collect { request_question_a.isBlank(it) } == [false] * 3
    selections.collect { request_question_b.isBlank(it) } == [false] * 3
    selections.collect { request_tiebreak.isBlank(it) } == [false, false, true]
  }

  def "Request.isBlank - an empty answer (non-eligible voter) should never be considered BLANK"() {
    given:
    def request_ballot_1 = reader.forQuestion("vote_1", "ballot_1", "ballot_1")
    def request_tiebreak = reader.forQuestion("vote_1", "ballot_2", "tiebreak")
    def selection = [false] * 10

    expect:
    request_ballot_1.isBlank(selection) == Boolean.FALSE
    request_tiebreak.isBlank(selection) == Boolean.FALSE
  }

  def "Request.isAnswered should return true for an empty answer (non-eligible voter)"() {
    given:
    def request = reader.forQuestion("vote_1", "ballot_1", "ballot_1")
    def selection_blank = [false, false, true] + [false] * 7
    def selection_non_eligible = [false] * 10

    expect:
    request.isAnswered(selection_blank) == Boolean.TRUE
    request.isAnswered(selection_non_eligible) == Boolean.FALSE
  }

}
