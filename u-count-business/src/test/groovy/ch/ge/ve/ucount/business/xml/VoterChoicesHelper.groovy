/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml

import ch.ge.ve.model.convert.model.AnswerTypeEnum
import ch.ge.ve.model.convert.model.VoterVotationChoice
import ch.ge.ve.protocol.model.Election
import groovy.transform.CompileStatic

/**
 * A simple static factory to create VoterChoice objects for tests.
 */
@CompileStatic
class VoterChoicesHelper {

  /**
   * Generate one VoterVotationChoice per answerType option, flagged as a standard ballot and defining the questionId
   * to be same as the ballotId
   */
  static List<VoterVotationChoice> forVotationStandardBallot(String voteId, String ballotId, AnswerTypeEnum answerType) {
    return questionVoterVotationChoices(voteId, ballotId, ballotId, answerType, true)
  }

  /**
   * Generate one VoterVotationChoice per answerType option, flagged as a variant ballot
   */
  static List<VoterVotationChoice> forVotationVariantBallot(String voteId, String ballotId, String questionId, AnswerTypeEnum answerType) {
    return questionVoterVotationChoices(voteId, ballotId, questionId, answerType, false)
  }

  /**
   * Generate one VoterVotationChoice per answerType option
   */
  static List<VoterVotationChoice> questionVoterVotationChoices(String voteId, String ballotId, String questionId, AnswerTypeEnum answerType, boolean isStandard) {
    def election = new Election(questionId, answerType.options.size(), 1, null)
    answerType.options.collect { opt ->
      new VoterVotationChoice(election, voteId, opt, ballotId, answerType, isStandard, !isStandard)
    }
  }

}
