/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml

import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer
import ch.ge.ve.javafx.business.json.JsonPathMapper
import ch.ge.ve.javafx.business.xml.HeaderTypeFactory
import ch.ge.ve.javafx.business.xml.VoterChoicesFactory
import ch.ge.ve.model.convert.api.VoterChoiceConverter
import ch.ge.ve.model.convert.impl.DefaultVoterChoiceConverter
import ch.ge.ve.ucount.business.xml.ech110.ECH0110Factory
import ch.ge.ve.ucount.business.xml.ech110.ECH0110Generator
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import java.security.cert.X509Certificate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.test.context.TestPropertySource

@Configuration
@TestPropertySource(locations = "classpath:application.properties")
@Import([HeaderTypeFactory, ECH0110Factory, VoterChoicesFactory, ECH0110Generator])
class XmlTestsConfiguration {

  @Bean
  VoterChoiceConverter voterChoiceConverter() {
    return new DefaultVoterChoiceConverter()
  }

  @Bean
  JsonPathMapper jsonPathMapper(ObjectMapper simpleObjectMapper) {
    return new JsonPathMapper(simpleObjectMapper)
  }

  @Bean
  ObjectMapper simpleObjectMapper() {
    def module = new SimpleModule()

    // only deserializer for this context
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer())

    return new ObjectMapper().registerModule(module)
  }

}
