/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110

import static ch.ge.ve.model.convert.model.AnswerTypeEnum.INITIATIVE_COUNTER_PROPOSITION_BLANK
import static ch.ge.ve.model.convert.model.AnswerTypeEnum.YES_NO_BLANK
import static ch.ge.ve.ucount.business.xml.VoterChoicesHelper.forVotationVariantBallot

import ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery
import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery
import ch.ge.ve.interfaces.ech.service.JAXBEchCodecImpl
import ch.ge.ve.javafx.business.xml.HeaderTypeFactory
import ch.ge.ve.protocol.model.CountingCircle
import ch.ge.ve.protocol.model.Tally
import ch.ge.ve.ucount.business.tally.TallyHelper
import ch.ge.ve.ucount.business.tally.model.TallyResult
import ch.ge.ve.ucount.business.xml.VoterChoicesHelper
import ch.ge.ve.ucount.business.xml.exception.InconsistentDataException
import java.time.LocalDate
import spock.lang.Shared
import spock.lang.Specification

class ECH0110FactoryTest extends Specification {

  def ech159Codec = new JAXBEchCodecImpl<>(Delivery)

  def factory = new ECH0110Factory(
          new HeaderTypeFactory("OCSIN-SIDP", "Test-Product", "0.0.1", "MSG"),
          "GroovyTest based on SpockFramework")

  @Shared
  def countingCircles = [
          new CountingCircle(1, "cc_@1", "First counting circle"),
          new CountingCircle(2, "cc_@2", "Second counting circle"),
          new CountingCircle(3, "cc_@3", "Third counting circle")
  ]

  def helper = new TallyHelper()

  static voterChoices_simplestContest() {
    return VoterChoicesHelper.forVotationStandardBallot("201806VP-FED-CH", "FED_CH_Q1", YES_NO_BLANK) +
            VoterChoicesHelper.forVotationStandardBallot("201806VP-FED-CH", "FED_CH_Q2", YES_NO_BLANK)
  }

  static voterChoices_complexContest() {
    return VoterChoicesHelper.forVotationStandardBallot("201806VP-FED-CH", "FED_CH_Q1", YES_NO_BLANK) +
            VoterChoicesHelper.forVotationStandardBallot("201806VP-FED-CH", "FED_CH_Q2", YES_NO_BLANK) +
            VoterChoicesHelper.forVotationStandardBallot("201806VP-FED-CH", "FED_CH_Q3", YES_NO_BLANK) +

            VoterChoicesHelper.forVotationStandardBallot("201806VP-COM-6621", "COM_6621_Q1", YES_NO_BLANK) +
            VoterChoicesHelper.forVotationStandardBallot("201806VP-COM-6621", "COM_6621_Q2", YES_NO_BLANK) +
            VoterChoicesHelper.forVotationStandardBallot("201806VP-COM-6621", "COM_6621_Q3", YES_NO_BLANK) +
            VoterChoicesHelper.forVotationStandardBallot("201806VP-COM-6621", "COM_6621_Q4", YES_NO_BLANK)
  }


  def "created Delivery should contain a header and some general contest information"() {
    given:
    def ech159Definition = openECH0159Definition("src/test/resources/ech0159/ech0110factory-test_ech159_simplest.xml" as File)

    def registeredVoters = [(countingCircles[0]): [CH: 1000L]]

    def voterChoices = voterChoices_simplestContest()
    def votes = helper.generateVotes(2, [3, 3])
    def tallyResult = helper.prepareTallyResult([(countingCircles[0]): votes])

    when:
    def result = factory.create(List.of(ech159Definition), registeredVoters, voterChoices, tallyResult)

    then: "there should be a header and some general contest information"
    result.deliveryHeader != null
    result.resultDelivery.reportingBody.reportingBodyIdentification == factory.@reportingAuthority
    result.resultDelivery.contestInformation.contestIdentification == "20180630VPGEN"
    result.resultDelivery.contestInformation.contestDate == LocalDate.of(2018, 6, 30)
  }


  def "create should regroup results per counting circle"() {
    given:
    def ech159Definition = openECH0159Definition("src/test/resources/ech0159/ech0110factory-test_ech159.xml" as File)

    def registeredVoters = [
            (countingCircles[0]): [CH: 1000L, '6621': 800L],
            (countingCircles[1]): [CH: 1500L, '6621': 950L],
            (countingCircles[2]): [CH: 2000L, '6621': 733L]
    ]
    def voterChoices = voterChoices_complexContest()
    //   --> 3 questions for "201806VP-FED-CH"  and  4 questions for "201806VP-COM-6621" --> 21 candidates
    def votes = [
            (countingCircles[0]): helper.generateVotes(200, [3] * 3 + [3] * 4),
            (countingCircles[1]): helper.generateVotes(550, [3] * 3 + [3] * 4),
            (countingCircles[2]): helper.generateVotes(661, [3] * 3 + [3] * 4)
    ]
    def tallyResult = helper.prepareTallyResult(votes)

    when:
    def result = factory.create(List.of(ech159Definition), registeredVoters, voterChoices, tallyResult)

    then: "there should be a section per counting circle => 3 sections"
    result.resultDelivery.countingCircleResults.size() == 3
    result.resultDelivery.countingCircleResults[0].countingCircle.countingCircleId == "cc_@1"
    result.resultDelivery.countingCircleResults[1].countingCircle.countingCircleId == "cc_@2"
    result.resultDelivery.countingCircleResults[2].countingCircle.countingCircleId == "cc_@3"

    and: "the number of votes cast per counting circle should be the number of voter ballots received"
    result.resultDelivery.countingCircleResults[0].votingCardsInformation.countOfReceivedValidVotingCardsTotal == 200
    result.resultDelivery.countingCircleResults[1].votingCardsInformation.countOfReceivedValidVotingCardsTotal == 550
    result.resultDelivery.countingCircleResults[2].votingCardsInformation.countOfReceivedValidVotingCardsTotal == 661

    and: "the votes must be organized in the order defined by VoterChoices"
    result.resultDelivery.countingCircleResults[0].voteResults[0].vote.voteIdentification == "201806VP-FED-CH"
    result.resultDelivery.countingCircleResults[0].voteResults[1].vote.voteIdentification == "201806VP-COM-6621"
    result.resultDelivery.countingCircleResults[1].voteResults[0].vote.voteIdentification == "201806VP-FED-CH"
    result.resultDelivery.countingCircleResults[1].voteResults[1].vote.voteIdentification == "201806VP-COM-6621"

    and: "the number of registered voters per vote should come from the number of voters having access to the domainOfInfluence"
    result.resultDelivery.countingCircleResults[0].voteResults[0].countOfVotersInformation.countOfVotersTotal == 1000
    result.resultDelivery.countingCircleResults[0].voteResults[1].countOfVotersInformation.countOfVotersTotal == 800
    result.resultDelivery.countingCircleResults[1].voteResults[0].countOfVotersInformation.countOfVotersTotal == 1500
    result.resultDelivery.countingCircleResults[1].voteResults[1].countOfVotersInformation.countOfVotersTotal == 950
    result.resultDelivery.countingCircleResults[2].voteResults[0].countOfVotersInformation.countOfVotersTotal == 2000
    result.resultDelivery.countingCircleResults[2].voteResults[1].countOfVotersInformation.countOfVotersTotal == 733
  }


  def "create should correctly append results for standard ballots"() {
    given:
    def ech159Definition = openECH0159Definition("src/test/resources/ech0159/ech0110factory-test_ech159_simplest.xml" as File)

    def registeredVoters = [(countingCircles[0]): [CH: 100L]]

    def voterChoices = voterChoices_simplestContest()
    def x = true, _ = false
    def votes = [
            [x, _, _] + [x, _, _],
            [_, x, _] + [x, _, _],
            [_, x, _] + [_, _, x]
    ]
    def tally = [1L, 2L, 0L, 2L, 0L, 1L]
    def tallyResult = helper.prepareTallyResult(countingCircles[0], votes, tally)


    when:
    def result = factory.create(List.of(ech159Definition), registeredVoters, voterChoices, tallyResult)

    then: "results should be correctly mapped for question FED_CH_Q1"
    def ballot_1 = result.resultDelivery.countingCircleResults[0].voteResults[0].ballotResult[0]
    ballot_1.ballotIdentification == "FED_CH_Q1"
    // global
    ballot_1.countOfReceivedBallotsTotal.total == 3
    ballot_1.countOfAccountedBallotsTotal.total == 3
    // detail
    ballot_1.standardBallot.countOfAnswerYes.total == 1
    ballot_1.standardBallot.countOfAnswerNo.total == 2
    ballot_1.standardBallot.countOfAnswerInvalid.total == 0
    ballot_1.standardBallot.countOfAnswerEmpty.total == 0

    and: "results should be correctly mapped for question FED_CH_Q2"
    def ballot_2 = result.resultDelivery.countingCircleResults[0].voteResults[0].ballotResult[1]
    ballot_2.ballotIdentification == "FED_CH_Q2"
    // global
    ballot_2.countOfReceivedBallotsTotal.total == 3
    ballot_2.countOfAccountedBallotsTotal.total == 3
    // detail
    ballot_2.standardBallot.countOfAnswerYes.total == 2
    ballot_2.standardBallot.countOfAnswerNo.total == 0
    ballot_2.standardBallot.countOfAnswerInvalid.total == 0
    ballot_2.standardBallot.countOfAnswerEmpty.total == 1
  }


  def "create should correctly append results for variant ballots"() {
    given: 'a votation with a single typical variant ballot (2 questions, 1 tiebreak)'
    def ech159Definition = openECH0159Definition("src/test/resources/ech0159/ech0110factory-test_ech159_variant.xml" as File)

    def registeredVoters = [(countingCircles[0]): [CH: 100L]]
    def voterChoices = forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q3", "FED_CH_Q3.a", YES_NO_BLANK) +
            forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q3", "FED_CH_Q3.b", YES_NO_BLANK) +
            forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q3", "FED_CH_Q3.c", INITIATIVE_COUNTER_PROPOSITION_BLANK)

    and: '5 selection vectors in the tally'
    def x = true, _ = false
    def selections = [
            [x, _, _] + [x, _, _] + [_, _, x],
            [_, x, _] + [x, _, _] + [_, x, _],
            [x, _, _] + [x, _, _] + [x, _, _],
            [_, x, _] + [_, _, x] + [_, _, x],
            [x, _, _] + [_, x, _] + [x, _, _]
    ]
    def tally = [3L, 2L, 0L, 3L, 1L, 1L, 2L, 1L, 2L]
    def tallyResult = helper.prepareTallyResult(countingCircles[0], selections, tally)

    when:
    def result = factory.create(List.of(ech159Definition), registeredVoters, voterChoices, tallyResult)

    then: 'the (single) ballot should be correctly identified'
    result.resultDelivery.countingCircleResults[0].voteResults[0].ballotResult.size() == 1
    def ballot = result.resultDelivery.countingCircleResults[0].voteResults[0].ballotResult[0]
    ballot.ballotIdentification == "FED_CH_Q3"
    ballot.standardBallot == null && ballot.variantBallot
    // global
    ballot.countOfReceivedBallotsTotal.total == 5
    ballot.countOfAccountedBallotsTotal.total == 5
    // questions
    ballot.variantBallot.questionInformation.size() == 2
    ballot.variantBallot.tieBreak.size() == 1

    and: "results should be correctly mapped for question FED_CH_Q3.a"
    def question_a = ballot.variantBallot.questionInformation[0]
    question_a.questionIdentification == "FED_CH_Q3.a"
    question_a.countOfAnswerYes.total == 3
    question_a.countOfAnswerNo.total == 2
    question_a.countOfAnswerEmpty.total == 0

    and: "results should be correctly mapped for question FED_CH_Q3.b"
    def question_b = ballot.variantBallot.questionInformation[1]
    question_b.questionIdentification == "FED_CH_Q3.b"
    question_b.countOfAnswerYes.total == 3
    question_b.countOfAnswerNo.total == 1
    question_b.countOfAnswerEmpty.total == 1

    and: "results should be correctly mapped for tie-break FED_CH_Q3.c"
    def tiebreak = ballot.variantBallot.tieBreak[0]
    tiebreak.questionIdentification == "FED_CH_Q3.c"
    tiebreak.countOfAnswerEmpty.total == 2
    tiebreak.countInFavourOf.size() == 2
    tiebreak.countInFavourOf[0].questionIdentification == "FED_CH_Q3.a"
    tiebreak.countInFavourOf[0].countOfValidAnswers.total == 2
    tiebreak.countInFavourOf[1].questionIdentification == "FED_CH_Q3.b"
    tiebreak.countInFavourOf[1].countOfValidAnswers.total == 1
  }

  def "create should correctly append results for multiple reference files"() {
    given:
    def ech159Definitions = [
            openECH0159Definition("src/test/resources/ech0159/ech0110factory-test_ech159_simplest.xml" as File),
            openECH0159Definition("src/test/resources/ech0159/ech0110factory-test_ech159_variant.xml" as File)
    ]

    def registeredVoters = [(countingCircles[0]): [CH: 100L]]

    def voterChoices = voterChoices_simplestContest() +
            forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q3", "FED_CH_Q3.a", YES_NO_BLANK) +
            forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q3", "FED_CH_Q3.b", YES_NO_BLANK) +
            forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q3", "FED_CH_Q3.c", INITIATIVE_COUNTER_PROPOSITION_BLANK)
    def x = true, _ = false
    def votes = [
            [x, _, _] + [x, _, _] + [x, _, _] + [x, _, _] + [_, _, x],
            [_, x, _] + [x, _, _] + [_, x, _] + [x, _, _] + [_, x, _],
            [_, x, _] + [_, _, x] + [x, _, _] + [x, _, _] + [x, _, _],
            [_, _, x] + [x, _, _] + [_, x, _] + [_, _, x] + [_, _, x],
            [_, x, _] + [_, _, x] + [x, _, _] + [_, x, _] + [x, _, _]
    ]
    def tally = [1L, 3L, 1L, 3L, 0L, 2L, 3L, 2L, 0L, 3L, 1L, 1L, 2L, 1L, 2L]
    def tallyResult = helper.prepareTallyResult(countingCircles[0], votes, tally)

    when:
    def result = factory.create(ech159Definitions, registeredVoters, voterChoices, tallyResult)

    then: "there should be two vote results"
    result.resultDelivery.countingCircleResults[0].voteResults.size() == 2
    result.resultDelivery.countingCircleResults[0].voteResults[0].vote.voteIdentification == "201806VP-FED-CH"
    result.resultDelivery.countingCircleResults[0].voteResults[0].ballotResult.size() == 2

    result.resultDelivery.countingCircleResults[0].voteResults[1].vote.voteIdentification == "VOTEID-FED-CH"
    result.resultDelivery.countingCircleResults[0].voteResults[1].ballotResult.size() == 1

    then: "results should be correctly mapped for question FED_CH_Q1"
    def ballot_1 = result.resultDelivery.countingCircleResults[0].voteResults[0].ballotResult[0]
    ballot_1.ballotIdentification == "FED_CH_Q1"
    // global
    ballot_1.countOfReceivedBallotsTotal.total == 5
    ballot_1.countOfAccountedBallotsTotal.total == 5
    // detail
    ballot_1.standardBallot.countOfAnswerYes.total == 1
    ballot_1.standardBallot.countOfAnswerNo.total == 3
    ballot_1.standardBallot.countOfAnswerInvalid.total == 0
    ballot_1.standardBallot.countOfAnswerEmpty.total == 1

    then: "results should be correctly mapped for question FED_CH_Q2"
    def ballot_2 = result.resultDelivery.countingCircleResults[0].voteResults[0].ballotResult[1]
    ballot_2.ballotIdentification == "FED_CH_Q2"
    // global
    ballot_2.countOfReceivedBallotsTotal.total == 5
    ballot_2.countOfAccountedBallotsTotal.total == 5
    // detail
    ballot_2.standardBallot.countOfAnswerYes.total == 3
    ballot_2.standardBallot.countOfAnswerNo.total == 0
    ballot_2.standardBallot.countOfAnswerInvalid.total == 0
    ballot_2.standardBallot.countOfAnswerEmpty.total == 2

    then: 'the variant ballot should be correctly identified'
    result.resultDelivery.countingCircleResults[0].voteResults[1].ballotResult.size() == 1
    def variantBallot = result.resultDelivery.countingCircleResults[0].voteResults[1].ballotResult[0]
    variantBallot.ballotIdentification == "FED_CH_Q3"
    variantBallot.standardBallot == null && variantBallot.variantBallot
    // global
    variantBallot.countOfReceivedBallotsTotal.total == 5
    variantBallot.countOfAccountedBallotsTotal.total == 5
    // questions
    variantBallot.variantBallot.questionInformation.size() == 2
    variantBallot.variantBallot.tieBreak.size() == 1

    and: "results should be correctly mapped for question FED_CH_Q3.a"
    def question_a = variantBallot.variantBallot.questionInformation[0]
    question_a.questionIdentification == "FED_CH_Q3.a"
    question_a.countOfAnswerYes.total == 3
    question_a.countOfAnswerNo.total == 2
    question_a.countOfAnswerEmpty.total == 0

    and: "results should be correctly mapped for question FED_CH_Q3.b"
    def question_b = variantBallot.variantBallot.questionInformation[1]
    question_b.questionIdentification == "FED_CH_Q3.b"
    question_b.countOfAnswerYes.total == 3
    question_b.countOfAnswerNo.total == 1
    question_b.countOfAnswerEmpty.total == 1

    and: "results should be correctly mapped for tie-break FED_CH_Q3.c"
    def tiebreak = variantBallot.variantBallot.tieBreak[0]
    tiebreak.questionIdentification == "FED_CH_Q3.c"
    tiebreak.countOfAnswerEmpty.total == 2
    tiebreak.countInFavourOf.size() == 2
    tiebreak.countInFavourOf[0].questionIdentification == "FED_CH_Q3.a"
    tiebreak.countInFavourOf[0].countOfValidAnswers.total == 2
    tiebreak.countInFavourOf[1].questionIdentification == "FED_CH_Q3.b"
    tiebreak.countInFavourOf[1].countOfValidAnswers.total == 1
  }


  def "create should fail if results are not consistent"() {
    given: 'the "simplest" election set'
    def ech159Definition = openECH0159Definition("src/test/resources/ech0159/ech0110factory-test_ech159_simplest.xml" as File)

    def countingCircle = new CountingCircle(1, "CC#1", "Unique counting circle")
    def registeredVoters = [(countingCircle): [CH: 100L]]
    def voterChoices = voterChoices_simplestContest()

    and: "a tally with a different total of voters per question (non-consistent)"
    def selections = helper.generateVotes(25, [3] * 2)
    def tally = helper.tallyFrom(selections)
    tally[5] = tally[5] + 2L  // 2nd question has now 27 answers -> tally is now inconsistent

    when: "the Delivery object is generated"
    def tallyResult = helper.prepareTallyResult(countingCircles[0], selections, tally)
    factory.create(List.of(ech159Definition), registeredVoters, voterChoices, tallyResult)

    then: "the generation should fail with an exception"
    def ex = thrown InconsistentDataException
    ex.message == '[CountingCircle="cc_@1", vote="201806VP-FED-CH"] All ballots should contain the same number of voters, but we found : [25, 27]'
  }


  def "create should fail if the tally result has inconsistent data regarding counting circles"() {
    given: 'the "simplest" election set and two participating couting circles'
    def ech159Definition = openECH0159Definition("src/test/resources/ech0159/ech0110factory-test_ech159_simplest.xml" as File)

    def registeredVoters = [(countingCircles[0]): [CH: 100L], (countingCircles[1]): [CH: 150L]]
    def voterChoices = voterChoices_simplestContest()

    and: "a tally result where votes are missing for the second counting circle"
    def selections_1 = helper.generateVotes(25, [3] * 2)
    def selections_2 = helper.generateVotes(20, [3] * 2)
    def tally = new Tally([
            (countingCircles[0]): helper.tallyFrom(selections_1),
            (countingCircles[1]): helper.tallyFrom(selections_2),
    ])
    def tallyResult = new TallyResult(null, null, [
            (countingCircles[0]): selections_1
    ], tally, null)

    when: "the Delivery object is generated"
    factory.create(List.of(ech159Definition), registeredVoters, voterChoices, tallyResult)

    then: "the generation should fail with an exception"
    def ex = thrown NullPointerException
    ex.message == 'No value associated to the countingCircle "cc_@2"'
  }


  def "create should fail if the definition of the counting circles has missing domain of influence information"() {
    given: 'the complex election set'
    def ech159Definition = openECH0159Definition("src/test/resources/ech0159/ech0110factory-test_ech159.xml" as File)

    def voterChoices = voterChoices_complexContest()
    //   --> 2 votes, one in domain "CH"  and  one in domain "6621"

    def votes = helper.generateVotes(10, [3] * 3 + [3] * 4)
    def tallyResult = helper.prepareTallyResult([(countingCircles[0]): votes])

    and: 'no information about the number of registered users for domain "6621"'
    def registeredVoters = [(countingCircles[0]): [CH: 100L]]

    when: "the Delivery object is generated"
    factory.create(List.of(ech159Definition), registeredVoters, voterChoices, tallyResult)

    then: "the generation should fail with an exception"
    def ex = thrown IllegalStateException
    ex.message == 'There are more votes cast than eligible voters (10 > 0) for counting circle "cc_@1" and domain of influence "6621"'
  }


  def "missing domain of influence information should be accepted if no vote has been cast"() {
    given: 'the complex election set'
    def ech159Definition = openECH0159Definition("src/test/resources/ech0159/ech0110factory-test_ech159.xml" as File)

    def voterChoices = voterChoices_complexContest()
    //   --> 2 votes, one in domain "CH"  and  one in domain "6621"

    and: 'no information about the number of registered users for domain "6621"'
    def registeredVoters = [(countingCircles[0]): [CH: 100L]]

    and: 'votes for "CH" domain only, no vote cast for domain "6621"'
    def emptyVotesFor6621 = [false] * 12    // 4 questions with 3 candidates => 12 choices
    def votes = helper.generateVotes(10, [3] * 3).collect { it + emptyVotesFor6621 }
    def tallyResult = helper.prepareTallyResult([(countingCircles[0]): votes])

    when: "the Delivery object is generated"
    factory.create(List.of(ech159Definition), registeredVoters, voterChoices, tallyResult)

    then: "the generation should fail with an exception"
    noExceptionThrown()
  }

  EventInitialDelivery openECH0159Definition(File source) {
    return source.withInputStream { ech159Codec.deserialize(it, false) }.initialDelivery
  }
}
