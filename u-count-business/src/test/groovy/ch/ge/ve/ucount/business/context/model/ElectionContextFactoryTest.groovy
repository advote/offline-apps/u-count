/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.context.model

import ch.ge.ve.javafx.business.json.JsonPathMapper
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey
import ch.ge.ve.protocol.model.ElectionSetForVerification
import ch.ge.ve.protocol.model.EncryptionPublicKey
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.ucount.business.TestUtils
import ch.ge.ve.ucount.business.context.model.ElectionContextFactory
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class ElectionContextFactoryTest extends Specification {

  @Rule
  public TemporaryFolder tempFolder = new TemporaryFolder()

  ElectionContextFactory electionContextFactory
  JsonPathMapper jsonPathMapper

  def setup() {
    electionContextFactory = new ElectionContextFactory(TestUtils.createRandomGenerator(), "BC", "BLAKE2B-256")
    jsonPathMapper = new JsonPathMapper(TestUtils.createObjectMapper())
  }

  def "should create an election context from a valid archive and a pair of election officer keys"() {
    given:
    def electionSet = TestUtils.RESOURCES_ROOT.resolve("election-context-factory").resolve("election-set-for-verification.json")
    def publicParams = TestUtils.RESOURCES_ROOT.resolve("election-context-factory").resolve("public-parameters.json")
    def eaPrivateKey = TestUtils.ELECTION_OFFICER_PRIVATE_KEY_FOLDER.resolve("election-officer-private-key.json")
    def eaPublicKey = TestUtils.ELECTION_OFFICER_PUBLIC_KEY_FOLDER.resolve("election-officer-public-key.json")

    when:
    def result = electionContextFactory.createElectionContext(
        jsonPathMapper.map(electionSet, ElectionSetForVerification.class),
        jsonPathMapper.map(publicParams, PublicParameters.class),
        jsonPathMapper.map(eaPrivateKey, EncryptionPrivateKey.class),
        jsonPathMapper.map(eaPublicKey, EncryptionPublicKey.class))

    then:
    result.electionSet != null
    result.electionSet.countingCircleCount == 2
    result.electionSet.candidates.size() == 27

    result.publicParameters != null
    result.electionOfficerPrivateKey != null
    result.electionOfficerPublicKey != null
    result.tallyingAuthoritiesAlgorithm != null
    result.decryptionAuthorityAlgorithms != null
    result.hash != null
  }
}
