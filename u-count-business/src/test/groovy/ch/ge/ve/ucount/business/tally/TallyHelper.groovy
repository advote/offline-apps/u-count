/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.tally

import ch.ge.ve.protocol.model.CountingCircle
import ch.ge.ve.protocol.model.Tally
import ch.ge.ve.ucount.business.tally.model.TallyResult
import groovy.transform.CompileStatic
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Helps generating votes and tally for tests
 */
@CompileStatic
class TallyHelper {

  private static final Logger logger = LoggerFactory.getLogger(TallyHelper)

  private final Random random = new Random()

  void setSeed(long seed) {
    this.random.setSeed(seed)
  }

// Ensure consistent tally data, whatever the size, with "generateVotes(..)" and "tallyFrom(..)"
  List<List<Boolean>> generateVotes(int voters, List<Integer> candidatesPerQuestion) {
    logger.debug("[seed={}] Generate {} votes with {} choices (first 5 displayed only) :",
            random.properties.get("seed"), voters, candidatesPerQuestion.sum())
    return (1..voters).collect {
      def answers = candidatesPerQuestion.collect { nbCandidates ->
        def choice = random.nextInt(nbCandidates)
        def maxIndex = nbCandidates - 1
        return (0..maxIndex).collect { it == choice }

      }.flatten() as List<Boolean>

      if (it < 5) {
        logger.debug("  ($it) - ${answers.collect { it ? "x" : "_" }}")
      }
      return answers
    }
  }

  // Ensure consistent tally data, whatever the size, with "generateVotes(..)" and "tallyFrom(..)"
  List<Long> tallyFrom(List<List<Boolean>> votes) {
    def maxIndex = votes.first().size() - 1
    def tally = (0..maxIndex).collect { index ->
      votes.count { it[index] } as Long
    }
    logger.debug("Calculated tally : {}", tally)
    return tally
  }

  TallyResult prepareTallyResult(Map<CountingCircle, List<List<Boolean>>> votesByCountingCircle) {
    Map<CountingCircle, List<Long>> tally = [:]
    votesByCountingCircle.each { cc, votes ->
      tally.put(cc, tallyFrom(votes))
    }

    return new TallyResult(null, null, votesByCountingCircle, new Tally(tally), null)
  }

  TallyResult prepareTallyResult(CountingCircle countingCircle, List<List<Boolean>> votes, List<Long> tally) {
    def votesMap = [(countingCircle): votes]
    def tallyMap = [(countingCircle): tally]
    return new TallyResult(null, null, votesMap, new Tally(tallyMap), null)
  }

}
