/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.key

import ch.ge.ve.javafx.business.json.JsonPathMapper
import ch.ge.ve.protocol.support.PublicParametersFactory
import ch.ge.ve.ucount.business.TestUtils
import ch.ge.ve.ucount.util.key.KeyPairFactoryDefaultImpl
import ch.ge.ve.ucount.util.key.KeyShareCodecPkcs12Impl
import ch.ge.ve.ucount.util.key.KeySharingScheme
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class ElectionOfficerKeyServiceTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder

  ElectionOfficerKeyService electionOfficerKeyService

  def setup() {
    electionOfficerKeyService = new ElectionOfficerKeyService(
            new JsonPathMapper(TestUtils.createObjectMapper()),
            PublicParametersFactory.LEVEL_2.createPublicParameters(),
            new KeyPairFactoryDefaultImpl(TestUtils.createRandomGenerator()),
            new KeyShareCodecPkcs12Impl())
  }

  def "should generate a key with 3 shares"() {
    given:
    KeySharingScheme scheme = KeySharingScheme.of(2, 3)

    when:
    def result = electionOfficerKeyService.generateElectionOfficerKey(scheme)

    then:
    result.publicKey != null
    result.privateKeyShares.size() == 3
  }

  def "should store a key share to the specified folder"() {
    given:
    KeySharingScheme scheme = KeySharingScheme.of(2, 3)
    def key = electionOfficerKeyService.generateElectionOfficerKey(scheme)
    def outputFolder = temporaryFolder.newFolder().toPath()

    when:
    def outputFile = electionOfficerKeyService.storeKeyShare(key.getPrivateKeyShares()[0], outputFolder, "password")

    then:
    outputFile.fileName.toString() =~ '^private-key-share_(.*)\\.pfx$'
    outputFile.parent == outputFolder
  }

  def "should store a public key to the specified folder"() {
    given:
    KeySharingScheme scheme = KeySharingScheme.of(1, 1)
    def key = electionOfficerKeyService.generateElectionOfficerKey(scheme)
    def outputFolder = temporaryFolder.newFolder().toPath()

    when:
    def outputFile = electionOfficerKeyService.storePublicKey(key.getPublicKey(), outputFolder)

    then:
    outputFile.fileName.toString() =~ '^public-key_(.*)\\.json'
    outputFile.parent == outputFolder
  }

  def "should recover the stored encryption key"() {
    given:
    KeySharingScheme scheme = KeySharingScheme.of(2, 3)
    def key = electionOfficerKeyService.generateElectionOfficerKey(scheme)

    when:
    def encryptionKeyPair = electionOfficerKeyService.recoverKeyPair(scheme, key)

    then:
    encryptionKeyPair.getPublicKey() == key.getPublicKey()
    encryptionKeyPair.getPrivateKey() != null
  }

  def "should recover the stored key share"() {
    given:
    KeySharingScheme scheme = KeySharingScheme.of(2, 3)
    def key = electionOfficerKeyService.generateElectionOfficerKey(scheme)
    def outputFolder = temporaryFolder.newFolder().toPath()
    def outputFile = electionOfficerKeyService.storeKeyShare(key.getPrivateKeyShares()[0], outputFolder, "password")

    when:
    def keyShare = electionOfficerKeyService.readKeyShare(outputFile, "password")

    then:
    new BigInteger(keyShare.publicKey()) == key.getPublicKey().publicKey
    keyShare == key.getPrivateKeyShares()[0]
  }
}
