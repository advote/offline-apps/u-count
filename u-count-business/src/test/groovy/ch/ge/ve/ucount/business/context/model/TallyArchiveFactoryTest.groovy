/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.context.model

import ch.ge.ve.model.convert.api.VoterChoiceConverter
import ch.ge.ve.ucount.business.TestUtils
import com.fasterxml.jackson.databind.ObjectMapper
import spock.lang.Specification

class TallyArchiveFactoryTest extends Specification {

  ObjectMapper objectMapper = Mock(ObjectMapper)

  VoterChoiceConverter voterChoiceConverter = Mock(VoterChoiceConverter)

  TallyArchiveFactory tallyArchiveFactory = new TallyArchiveFactory(objectMapper, voterChoiceConverter)


  def "should create a tally archive based on the mapper / converter it holds"() {
    given:
    def source = TestUtils.TALLY_ARCHIVE_FOLDER.resolve("tally-archive_valid.taf")

    when:
    def archive = tallyArchiveFactory.readTallyArchive(source)

    then:
    archive.operationName == "valid"
    archive.creationDate == Optional.empty()

    and:
    archive.objectMapper.is( objectMapper )
    archive.voterChoiceConverter.is( voterChoiceConverter )
  }
}