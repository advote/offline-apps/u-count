/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.tally

import ch.ge.ve.javafx.business.json.JsonPathMapper
import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.protocol.model.Tally
import ch.ge.ve.ucount.business.TestUtils
import ch.ge.ve.ucount.business.context.model.TallyContext
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class TallyGeneratorTest extends Specification {

  @Rule
  public TemporaryFolder tempFolder = new TemporaryFolder()

  TallyGenerator generator
  JsonPathMapper jsonPathMapper

  def setup() {
    jsonPathMapper = new JsonPathMapper(TestUtils.createObjectMapper())
    generator = new TallyGenerator()
  }

  def "should calculate the tally for a valid archive"() {
    given:
    def tallyArchivePath = TestUtils.TALLY_ARCHIVE_FOLDER.resolve("tally-archive_valid.taf")
    def tallyArchive = TestUtils.createTallyArchive(tallyArchivePath)
    def electionContext = TestUtils.createElectionContext(tallyArchive)
    def tallyContext = TallyContext.from(tallyArchive)
    def expectedTally = jsonPathMapper.map(
            TestUtils.EXPECTED_TALLY_FOLDER.resolve("valid-archive-expected-tally.json"),
            Tally.class)

    when:
    def result = generator.generateTally(
            electionContext,
            tallyContext.getPrimes(),
            tallyContext.getFinalShuffle(),
            tallyContext.getControlComponentsPartialDecryptions(),
            tallyContext.getCountingCircles(),
            Mock(ProgressTracker))

    then:
    expectedTally == result.getTally()
  }
}
