/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.context.model;

import ch.ge.ve.protocol.core.algorithm.DecryptionAuthorityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.GeneralAlgorithms;
import ch.ge.ve.protocol.core.algorithm.TallyingAuthoritiesAlgorithm;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.PrimesCache;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * A factory for creating {@link ElectionContext} instances.
 */
@Service
public class ElectionContextFactory {
  private final RandomGenerator randomGenerator;
  private final String          digestProvider;
  private final String          digestAlgorithm;

  /**
   * Create a new {@link ElectionContext} factory instance.
   *
   * @param randomGenerator a cryptographically secured random generator.
   * @param digestProvider  the name of the digest provider.
   * @param digestAlgorithm the name of the digest algorithm.
   */
  @Autowired
  public ElectionContextFactory(RandomGenerator randomGenerator,
                                @Value("${ucount.digest-provider}") String digestProvider,
                                @Value("${ucount.digest-algorithm}") String digestAlgorithm) {
    this.randomGenerator = randomGenerator;
    this.digestProvider = digestProvider;
    this.digestAlgorithm = digestAlgorithm;
  }

  /**
   * Create a new {@link ElectionContext} context initialized with the given parameters.
   *
   * @param electionSet               the election set.
   * @param publicParameters          the public parameters.
   * @param electionOfficerPrivateKey the election officer private key.
   * @param electionOfficerPublicKey  the election officer public key.
   *
   * @return the initialized {@link ElectionContext}.
   */
  public ElectionContext createElectionContext(ElectionSetForVerification electionSet,
                                               PublicParameters publicParameters,
                                               EncryptionPrivateKey electionOfficerPrivateKey,
                                               EncryptionPublicKey electionOfficerPublicKey) {

    Hash hash = createHash(publicParameters);
    GeneralAlgorithms generalAlgorithms = createGeneralAlgorithms(electionSet, publicParameters, hash);

    return new ElectionContext(
        electionSet,
        publicParameters,
        electionOfficerPrivateKey,
        electionOfficerPublicKey,
        new TallyingAuthoritiesAlgorithm(publicParameters, generalAlgorithms),
        new DecryptionAuthorityAlgorithms(publicParameters, generalAlgorithms, randomGenerator),
        hash
    );

  }

  private Hash createHash(PublicParameters publicParameters) {
    return new Hash(digestAlgorithm, digestProvider, publicParameters.getSecurityParameters().getUpper_l());
  }

  private static GeneralAlgorithms createGeneralAlgorithms(ElectionSetForVerification electionSet,
                                                           PublicParameters publicParameters,
                                                           Hash hash) {
    int countingCircleCount = electionSet.getCountingCircleCount();
    try {
      PrimesCache primesCache = PrimesCache.populate(electionSet.getCandidates().size() + countingCircleCount,
                                                     publicParameters.getEncryptionGroup());
      return new GeneralAlgorithms(hash, publicParameters.getEncryptionGroup(),
                                   publicParameters.getIdentificationGroup(), primesCache);
    } catch (NotEnoughPrimesInGroupException e) {
      throw new IllegalStateException("Cannot instantiate general algorithm", e);
    }
  }
}
