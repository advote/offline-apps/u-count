/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.tally.model;

import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.Tally;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * An object holding the result of a tally generation process.
 */
public class TallyResult {
  private final Decryptions                              electionOfficerDecryptions;
  private final DecryptionProof                          electionOfficerDecryptionProof;
  private final Map<CountingCircle, List<List<Boolean>>> castedVotesByCountingCircle;
  private final Tally                                    tally;
  private final Duration                                 elapsedTime;

  /**
   * Create a new {@link TallyResult} intance.
   *
   * @param electionOfficerDecryptions     the partial encryption obtained with the election officer key.
   * @param electionOfficerDecryptionProof the proof of the elections officer decryption.
   * @param tally                          the tally.
   * @param castedVotesByCountingCircle    the map of casted votes by counting circle.
   * @param elapsedTime                    the it took to decrypt and compute the tally.
   */
  public TallyResult(Decryptions electionOfficerDecryptions,
                     DecryptionProof electionOfficerDecryptionProof,
                     Map<CountingCircle, List<List<Boolean>>> castedVotesByCountingCircle,
                     Tally tally,
                     Duration elapsedTime) {

    this.electionOfficerDecryptions = electionOfficerDecryptions;
    this.electionOfficerDecryptionProof = electionOfficerDecryptionProof;
    this.castedVotesByCountingCircle = castedVotesByCountingCircle;
    this.tally = tally;
    this.elapsedTime = elapsedTime;
  }

  public Decryptions getElectionOfficerDecryptions() {
    return electionOfficerDecryptions;
  }

  public DecryptionProof getElectionOfficerDecryptionProof() {
    return electionOfficerDecryptionProof;
  }

  public Map<CountingCircle, List<List<Boolean>>> getCastedVotesByCountingCircle() {
    return castedVotesByCountingCircle;
  }

  /**
   * Verifies if at least one vote has been casted
   *
   * @param countingCircle the countingCircle to check
   *
   * @return {@code true} if at least one selection contains at least one "true"
   */
  public boolean hasAnyVoteBeenCasted(CountingCircle countingCircle) {
    return castedVotesByCountingCircle.getOrDefault(countingCircle, Collections.emptyList())
                                      .parallelStream()
                                      .flatMap(List::stream).anyMatch(Boolean::booleanValue);
  }

  public Tally getTally() {
    return tally;
  }

  public Duration getElapsedTime() {
    return elapsedTime;
  }
}
