/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern;

class TieBreakQuestionResult {

  private final String    questionIdentification;
  private final Candidate tallyFirstQuestion;
  private final Candidate tallySecondQuestion;
  private final long      tallyBlanks;

  public TieBreakQuestionResult(String questionIdentification,
                                Candidate tallyFirstQuestion,
                                Candidate tallySecondQuestion,
                                long tallyBlanks) {
    this.questionIdentification = questionIdentification;
    this.tallyFirstQuestion = tallyFirstQuestion;
    this.tallySecondQuestion = tallySecondQuestion;
    this.tallyBlanks = tallyBlanks;
  }

  public String getQuestionIdentification() {
    return questionIdentification;
  }

  public Candidate getTallyFirstQuestion() {
    return tallyFirstQuestion;
  }

  public Candidate getTallySecondQuestion() {
    return tallySecondQuestion;
  }

  public long getTallyBlanks() {
    return tallyBlanks;
  }

  /**
   * Helper class to structure a candidate's data
   */
  static class Candidate {
    private final String questionIdentification;
    private final long   count;

    public Candidate(String questionIdentification, long count) {
      this.questionIdentification = questionIdentification;
      this.count = count;
    }

    public String getQuestionIdentification() {
      return questionIdentification;
    }

    public long getCount() {
      return count;
    }
  }

}
