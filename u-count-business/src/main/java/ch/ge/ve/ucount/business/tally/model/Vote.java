/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.tally.model;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Objects;

/**
 * Simple business representation of a vote
 */
public class Vote {

  private final String       id;
  private final List<Ballot> ballots;
  private       Contest      contest;

  public Vote(String id, List<Ballot> ballots) {
    this.id = id;
    this.ballots = ImmutableList.copyOf(ballots.stream().peek(b -> b.setVote(this)).iterator());
  }

  // non public as "contest" is logically immutable. It is not enforced with "final" for construction simplification.
  void setContest(Contest contest) {
    if (this.contest != null) {
      throw new IllegalStateException("The contest is already set to " + this.contest);
    }
    this.contest = Objects.requireNonNull(contest, "Contest must be specified");
  }

  public Contest getContest() {
    return contest;
  }

  public String getId() {
    return id;
  }

  public List<Ballot> getBallots() {
    return ballots;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Vote vote = (Vote) o;
    return Objects.equals(id, vote.id) &&
           Objects.equals(nullsafeContestId(), vote.nullsafeContestId()) && // only the id to avoid cyclic equals
           Objects.equals(ballots, vote.ballots);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, ballots, nullsafeContestId());
  }

  private String nullsafeContestId() {
    return contest != null ? contest.getId() : null;
  }
}
