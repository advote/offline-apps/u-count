/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern;

import ch.ge.ve.interfaces.ech.eCH0110.v4.BallotResultType;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Component that helps applying the results of a "standard ballot".
 * <p>
 * This is the simplest case, as a standard ballot is constituted of a single question.
 * </p>
 */
class StandardBallotResult extends AbstractBallotAnswerResult {

  private final QuestionResult questionResult;

  public StandardBallotResult(QuestionResult questionResult) {
    this.questionResult = questionResult;
  }

  public QuestionResult getQuestionResult() {
    return questionResult;
  }

  @Override
  void appendResultTo(BallotResultType echBallotResult, Map<AnswerTypeEnum, AnswerTypeAppender> appenders) {
    // Supposed that ballotIdentification, position etc. already set by caller
    echBallotResult.setStandardBallot(createStandardBallotResultTag(questionResult, appenders));
  }

  /**
   * Blank results are described at the question's level rather than at the ballot's.
   *
   * @return 0
   */
  @Override
  long getNumberOfBlanks() {
    return 0;
  }

  @Override
  long getNumberOfVoters() {
    return Stream.of(questionResult.getTallyByAnswerOption()).mapToLong(Long::longValue).sum();
  }
}
