/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.context.exception;

import ch.ge.ve.javafx.business.exception.ErrorCodeBasedException;

/**
 * Thrown when a required entry is not found while parsing a tally archive.
 */
public class MissingTallyEntriesException extends ErrorCodeBasedException {
  private static final String ERROR_CODE = "MISSING_TALLY_ENTRIES";

  /**
   * Create a new invalid tally entries exception.
   *
   * @param message the detail message.
   *
   * @see RuntimeException#RuntimeException(String)
   */
  public MissingTallyEntriesException(String message) {
    super(ERROR_CODE, message);
  }

}
