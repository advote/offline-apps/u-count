/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech222;

import ch.ge.ve.filenamer.FileNamer;
import ch.ge.ve.interfaces.ech.eCH0222.v1.Delivery;
import ch.ge.ve.interfaces.ech.service.EchSchema;
import ch.ge.ve.javafx.business.progress.IncrementalProgressNotifier;
import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.javafx.business.xml.HeaderTypeFactory;
import ch.ge.ve.model.convert.model.VoterVotationChoice;
import ch.ge.ve.ucount.business.context.model.TallyArchive;
import ch.ge.ve.ucount.business.tally.VotesCastedReader;
import ch.ge.ve.ucount.business.tally.model.Contest;
import ch.ge.ve.ucount.business.tally.model.ContestMapper;
import ch.ge.ve.ucount.business.tally.model.TallyResult;
import ch.ge.ve.ucount.business.xml.exception.ECHGenerationException;
import java.io.InputStream;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.xpath.XPathFactory;
import org.codehaus.stax2.XMLOutputFactory2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

/**
 * A service to generate an eCH-0222.
 */
@Service
public class ECH0222Generator {

  private final HeaderTypeFactory headerTypeFactory;
  private final String            reportingAuthority;

  private final JAXBContext      jaxbContext;
  private final XMLOutputFactory xmlOutputFactory;

  /**
   * Create a new {@link ECH0222Generator} instance.
   *
   * @param headerTypeFactory  factory that generates the eCH header
   * @param reportingAuthority name of the reporting authority to write in the eCH
   */
  @Autowired
  public ECH0222Generator(HeaderTypeFactory headerTypeFactory,
                          @Value("${ech.reporting.authority.name}") String reportingAuthority) {
    this.headerTypeFactory = headerTypeFactory;
    this.reportingAuthority = reportingAuthority;

    try {
      this.jaxbContext = JAXBContext.newInstance(Delivery.class);
      this.xmlOutputFactory = XMLOutputFactory2.newFactory();
    } catch (JAXBException e) {
      throw ECHGenerationException.forEch0222("Cannot initialize eCH-0222 generator", e);
    }
  }

  /**
   * Generate an eCH-0222 and saves to the given destination with the given paraemetrs.
   *
   * @param tallyArchive    the {@link TallyArchive} used to calculate the tally.
   * @param tallyResult     the {@link TallyResult}.
   * @param outputFolder    the folder where the eCH-0222 will be written to.
   * @param progressTracker a {@link ProgressTracker} strategy.
   *
   * @return the path to the written eCH-0222 file
   */
  public Path generateECH0222(TallyArchive tallyArchive,
                              TallyResult tallyResult,
                              Path outputFolder,
                              ProgressTracker progressTracker) {

    List<VoterVotationChoice> voterChoices = tallyArchive.recreateVoterChoices().stream()
                                                         // we only supplied votation's reference files :
                                                         .map(VoterVotationChoice.class::cast)
                                                         .collect(Collectors.toList());

    final Contest contest = ContestMapper.from(readContestIdentification(tallyArchive), voterChoices);
    final VotesCastedReader reader = new VotesCastedReader(voterChoices);

    IncrementalProgressNotifier tracker = new IncrementalProgressNotifier(
        progressTracker, tallyResult.getCastedVotesByCountingCircle().size());

    final Path destination = outputFolder.resolve(
        FileNamer.ech222File(tallyArchive.getOperationName(), LocalDateTime.now()));
    final ECH0222Writer ech0222Writer = new ECH0222Writer(jaxbContext, xmlOutputFactory, contest, reader);
    ech0222Writer.write(reportingAuthority, headerTypeFactory.createHeader(), tallyResult, tracker, destination);

    EchSchema.validate(destination);
    progressTracker.updateProgress(1, 1);
    return destination;
  }

  private static String readContestIdentification(TallyArchive tallyArchive) {
    final Map<String, Supplier<InputStream>> operationReferences = tallyArchive.getOperationReferences();

    // Note : the first contest definition is set since all of the files must have the same
    try (InputStream inputStream = operationReferences.values().iterator().next().get()) {
      return XPathFactory.newInstance().newXPath()
                         .compile("//*[local-name()='contestIdentification']/text()")
                         .evaluate(new InputSource(inputStream));
    } catch (Exception e) {
      final String entryName = operationReferences.keySet().iterator().next();
      throw ECHGenerationException.forEch0222("Unable to read contest identification from " + entryName, e);
    }
  }

}
