/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.tally;

import ch.ge.ve.javafx.business.progress.IncrementalProgressNotifier;
import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.protocol.core.algorithm.TallyingAuthoritiesAlgorithm;
import ch.ge.ve.protocol.core.exception.EncryptionColouringRuntimeException;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.support.MoreCollectors;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.ucount.business.context.model.ElectionContext;
import ch.ge.ve.ucount.business.tally.exception.InvalidDecryptionProofException;
import ch.ge.ve.ucount.business.tally.model.TallyResult;
import com.google.common.base.Stopwatch;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.stereotype.Service;

/**
 * A service to decrypt and compute the tally of an election.
 */
@Service
public class TallyGenerator {

  /**
   * Decrypt the ballots with the election officer private key and computes the tally.
   *
   * @param electionContext                     the election context, containing all the required election parameters.
   * @param finalShuffle                        the encrypted ballots.
   * @param controlComponentsPartialDecryptions the partial decryptions of each control component.
   * @param countingCircles                     the counting circles for the election.
   * @param progressTracker                     a {@link ProgressTracker} strategy
   *
   * @return An object containing the tally and the decryption proof.
   *
   * @throws InvalidDecryptionProofException if the computed decryption proof is not valid.
   */
  public TallyResult generateTally(ElectionContext electionContext,
                                   List<BigInteger> primes,
                                   List<Encryption> finalShuffle,
                                   List<Decryptions> controlComponentsPartialDecryptions,
                                   List<CountingCircle> countingCircles,
                                   ProgressTracker progressTracker) {
    Stopwatch stopwatch = Stopwatch.createStarted();
    IncrementalProgressNotifier tracker = new IncrementalProgressNotifier(progressTracker, 5);

    ElectionSetForVerification electionSet = electionContext.getElectionSet();
    TallyingAuthoritiesAlgorithm tallyingAuthoritiesAlgorithms = electionContext.getTallyingAuthoritiesAlgorithm();

    // performs the election officer decryption
    Decryptions electionOfficerDecryptions = generateElectionOfficerDecryptions(electionContext, finalShuffle);
    DecryptionProof electionOfficerDecryptionProof =
        generateElectionOfficerDecryptionProof(electionOfficerDecryptions, electionContext, finalShuffle);
    tracker.increment();

    // checks that the generated decryption proof is valid
    if (!checkElectionOfficerDecryptionProof(electionOfficerDecryptionProof,
                                             electionOfficerDecryptions,
                                             electionContext,
                                             finalShuffle)) {

      throw new InvalidDecryptionProofException("The election office decryption proof is not valid, aborting tally.");
    }
    tracker.increment();

    // combines the election officer decryptions with the control components decryptions
    List<Decryptions> partialDecryptions = new ArrayList<>(controlComponentsPartialDecryptions);
    partialDecryptions.add(electionOfficerDecryptions);
    Decryptions decryptions = tallyingAuthoritiesAlgorithms.getDecryptions(finalShuffle, partialDecryptions);
    tracker.increment();

    // retrieves the counting circle colouring from the decrypted votes
    int candidateCount = electionSet.getCandidates().size();

    Map<BigInteger, CountingCircle> countingCircleByColour =
        countingCircles.stream()
                       .distinct()
                       .collect(Collectors.toMap(
                           countingCircle -> primes.get(candidateCount + countingCircle.getId()),
                           countingCircle -> countingCircle)
                       );

    Map<CountingCircle, Decryptions> decryptionsByCountingCircle = decryptions
        .getDecryptionsList()
        .parallelStream()
        .collect(
            Collectors.groupingByConcurrent(
                extractCountingCircleFromDecryption(countingCircleByColour),
                MoreCollectors.toDecryptions()));
    tracker.increment();

    // runs the tallying for each counting circle
    Map<CountingCircle, List<List<Boolean>>> votes = decryptionsByCountingCircle.entrySet().parallelStream().collect(
        Collectors.toMap(
            Map.Entry::getKey,
            e -> tallyingAuthoritiesAlgorithms.getVotes(e.getValue(), candidateCount)
        )
    );

    Map<CountingCircle, List<Long>> finalTally = votes
        .entrySet().parallelStream()
        .collect(
            Collectors.toMap(
                Map.Entry::getKey,
                toVoteCountByCandidate(candidateCount)
            ));
    tracker.increment();

    return new TallyResult(electionOfficerDecryptions,
                           electionOfficerDecryptionProof,
                           votes,
                           new Tally(finalTally),
                           stopwatch.stop().elapsed());
  }

  private Decryptions generateElectionOfficerDecryptions(ElectionContext electionContext,
                                                         List<Encryption> finalShuffle) {

    EncryptionPrivateKey electionOfficerPrivateKey = electionContext.getElectionOfficerPrivateKey();
    return electionContext.getDecryptionAuthorityAlgorithms()
                          .getPartialDecryptions(finalShuffle,
                                                 electionOfficerPrivateKey.getPrivateKey());
  }

  private DecryptionProof generateElectionOfficerDecryptionProof(Decryptions electionOfficerDecryptions,
                                                                 ElectionContext electionContext,
                                                                 List<Encryption> finalShuffle) {

    EncryptionPrivateKey electionOfficerPrivateKey = electionContext.getElectionOfficerPrivateKey();
    EncryptionPublicKey electionOfficerPublicKey = electionContext.getElectionOfficerPublicKey();

    return electionContext.getDecryptionAuthorityAlgorithms()
                          .genDecryptionProof(electionOfficerPrivateKey.getPrivateKey(),
                                              electionOfficerPublicKey.getPublicKey(),
                                              finalShuffle,
                                              electionOfficerDecryptions);
  }

  private boolean checkElectionOfficerDecryptionProof(DecryptionProof electionOfficerDecryptionProof,
                                                      Decryptions decryptions,
                                                      ElectionContext electionContext,
                                                      List<Encryption> finalShuffle) {

    TallyingAuthoritiesAlgorithm tallyingAuthoritiesAlgorithms = electionContext.getTallyingAuthoritiesAlgorithm();
    EncryptionPublicKey electionOfficerPublicKey = electionContext.getElectionOfficerPublicKey();

    return tallyingAuthoritiesAlgorithms.checkDecryptionProof(electionOfficerDecryptionProof,
                                                              electionOfficerPublicKey.getPublicKey(),
                                                              finalShuffle,
                                                              decryptions);

  }

  private Function<BigInteger, CountingCircle> extractCountingCircleFromDecryption(
      Map<BigInteger, CountingCircle> countingCircleByColour) {

    return decryption ->
        countingCircleByColour
            .entrySet()
            .parallelStream()
            .filter(e -> decryption.mod(e.getKey()).equals(BigInteger.ZERO))
            .findFirst()
            .map(Map.Entry::getValue)
            .orElseThrow(
                () -> new EncryptionColouringRuntimeException("Failed to retrieve color from ballot")
            );
  }

  private Function<Map.Entry<CountingCircle, List<List<Boolean>>>, List<Long>> toVoteCountByCandidate(
      int candidateCount) {

    return e -> IntStream
        .range(0, candidateCount)
        .mapToLong(
            i -> e.getValue()
                  .parallelStream()
                  .filter(vote -> vote.get(i))
                  .count()
        )
        .boxed()
        .collect(Collectors.toList());
  }
}
