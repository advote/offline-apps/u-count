/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern;

import ch.ge.ve.model.convert.model.AnswerTypeEnum;

/**
 * Regroup data about the results for a specific question.
 */
class QuestionResult {

  private final String         questionIdentification;
  private final AnswerTypeEnum answerType;
  private final Long[]         tallyByAnswerOption;

  public QuestionResult(String questionIdentification, AnswerTypeEnum answerType, Long[] tallyByAnswerOption) {
    this.questionIdentification = questionIdentification;
    this.answerType = answerType;
    this.tallyByAnswerOption = tallyByAnswerOption;
  }

  public String getQuestionIdentification() {
    return questionIdentification;
  }

  public AnswerTypeEnum getAnswerType() {
    return answerType;
  }

  public Long[] getTallyByAnswerOption() {
    return tallyByAnswerOption;
  }
}
