/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech222;

import ch.ge.ve.interfaces.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.ech.eCH0222.v1.ReportingBodyType;
import ch.ge.ve.interfaces.ech.eCH0222.v1.VoteRawDataType.BallotRawData.BallotCasted;
import ch.ge.ve.javafx.business.progress.IncrementalProgressNotifier;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.ucount.business.tally.VotesCastedReader;
import ch.ge.ve.ucount.business.tally.model.Ballot;
import ch.ge.ve.ucount.business.tally.model.Contest;
import ch.ge.ve.ucount.business.tally.model.Question;
import ch.ge.ve.ucount.business.tally.model.TallyResult;
import ch.ge.ve.ucount.business.tally.model.Vote;
import ch.ge.ve.ucount.business.xml.EchTagsUtils;
import ch.ge.ve.ucount.business.xml.exception.ECHGenerationException;
import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;

/**
 * A writer that helps the {@link ECH0222Generator} to produce the file for a specific archive.
 * <p>
 * The writer works in streaming, writing data as it is processed, so that very long files with many
 * votes can be written without memory issues.
 * </p>
 * <p>
 * This writer holds the values, such as the {@link Contest} definition, the {@link VotesCastedReader},
 * so that they do not need be passed as parameters from one method to another.
 * </p>
 */
class ECH0222Writer {

  private static final String ECH_0222_NS = "http://www.ech.ch/xmlns/eCH-0222/1";
  private static final String ECH_0222_P  = "e222";

  private static final Map<String, String> NAMESPACES = ImmutableMap.<String, String>builderWithExpectedSize(6)
      .put(ECH_0222_P, ECH_0222_NS)
      .put("e58", "http://www.ech.ch/xmlns/eCH-0058/5")
      .put("e8", "http://www.ech.ch/xmlns/eCH-0008/3")
      .put("e10", "http://www.ech.ch/xmlns/eCH-0010/6")
      .put("e44", "http://www.ech.ch/xmlns/eCH-0044/4")
      .put("e155", "http://www.ech.ch/xmlns/eCH-0155/4")
      .build();

  private static final String DELIVERY                 = "delivery";
  private static final String HEADER                   = "deliveryHeader";
  private static final String REPORTING_BODY           = "reportingBody";
  private static final String RAW_DATA_DELIVERY        = "rawDataDelivery";
  private static final String RAW_DATA                 = "rawData";
  private static final String CONTEST_IDENTIFICATION   = "contestIdentification";
  private static final String COUNTING_CIRCLE_RAW_DATA = "countingCircleRawData";
  private static final String COUNTING_CIRCLE_ID       = "countingCircleId";
  private static final String VOTE_RAW_DATA            = "voteRawData";
  private static final String VOTE_IDENTIFICATION      = "voteIdentification";
  private static final String BALLOT_RAW_DATA          = "ballotRawData";
  private static final String BALLOT_IDENTIFICATION    = "ballotIdentification";
  private static final String BALLOT_CASTED            = "ballotCasted";

  private final JAXBContext      jaxbContext;
  private final XMLOutputFactory xmlOutputFactory;
  private final XMLEventFactory  eventFactory;

  private final Contest           contest;
  private final VotesCastedReader reader;

  private Writer writer;

  ECH0222Writer(JAXBContext jaxbContext, XMLOutputFactory xmlOutputFactory,
                Contest contest, VotesCastedReader reader) {
    this.jaxbContext = jaxbContext;
    this.xmlOutputFactory = xmlOutputFactory;
    this.contest = contest;
    this.reader = reader;

    this.eventFactory = XMLEventFactory.newInstance();
  }

  /**
   * Write the eCH-222 file, in a streaming manner.
   *
   * @param reportingAuthority the reporting authority to indicate
   * @param headers            full eCH headers object
   * @param tallyResult        results to be written down
   * @param tracker            an incremental tracker to indicate what remains
   * @param outputFile         where to write the file
   */
  public void write(String reportingAuthority, HeaderType headers, TallyResult tallyResult,
                    IncrementalProgressNotifier tracker, Path outputFile) {
    try (OutputStream out = Files.newOutputStream(outputFile)) {
      writer = new Writer(xmlOutputFactory.createXMLEventWriter(out, StandardCharsets.UTF_8.name()));

      writer.writeHeaders(headers);

      writer.writeStartElement(RAW_DATA_DELIVERY);
      writer.writeReportingBody(reportingAuthority);

      writer.writeStartElement(RAW_DATA);
      writer.writeElement(CONTEST_IDENTIFICATION, contest.getId());

      tallyResult.getCastedVotesByCountingCircle().forEach(
          (countingCircle, votes) -> {
            if (tallyResult.hasAnyVoteBeenCasted(countingCircle)) {
              writeCountingCircleRawData(countingCircle, votes);
            }
            tracker.increment();
          }
      );

      writer.writeEndElement(RAW_DATA);
      writer.writeEndElement(RAW_DATA_DELIVERY);
      writer.writeEndElement(DELIVERY);
      writer.writeEndDocument();

    } catch (XMLStreamException | JAXBException | IOException e) {
      throw ECHGenerationException.forEch0222(
          String.format("A problem occurred while writing the eCH-0222 file to [%s]", outputFile), e);
    } finally {
      writer = null;
    }
  }

  private boolean hasAnyVoteBeenCasted(Ballot ballot, List<List<Boolean>> votesByVoter) {
    return ballot.getQuestions().parallelStream()
                 .map(this::createRequest)
                 .anyMatch(request -> votesByVoter.parallelStream().anyMatch(request::isAnswered));
  }

  private VotesCastedReader.Request createRequest(Question question) {
    return reader.forQuestion(question.getBallot().getVote().getId(), question.getBallot().getId(), question.getId());
  }

  private void writeCountingCircleRawData(CountingCircle countingCircle, List<List<Boolean>> votesByVoter) {
    try {
      writer.writeStartElement(COUNTING_CIRCLE_RAW_DATA);
      writer.writeElement(COUNTING_CIRCLE_ID, countingCircle.getBusinessId());

      contest.getVotes().stream()
             .filter(vote ->
                         vote.getBallots().parallelStream()
                             .anyMatch(ballot -> hasAnyVoteBeenCasted(ballot, votesByVoter))
             )
             .forEach(vote -> writeVoteRawData(votesByVoter, vote));

      writer.writeEndElement(COUNTING_CIRCLE_RAW_DATA);
    } catch (XMLStreamException e) {
      throw ECHGenerationException.forEch0222("Failed to write data for the countingCircle " + countingCircle, e);
    }
  }

  private void writeVoteRawData(List<List<Boolean>> votesByVoter, Vote vote) {
    try {
      writer.writeStartElement(VOTE_RAW_DATA);
      writer.writeElement(VOTE_IDENTIFICATION, vote.getId());

      vote.getBallots().stream()
          .filter(ballot -> hasAnyVoteBeenCasted(ballot, votesByVoter))
          .forEach(ballot -> writeBallotRawData(votesByVoter, ballot));

      writer.writeEndElement(VOTE_RAW_DATA);
    } catch (XMLStreamException e) {
      throw ECHGenerationException.forEch0222("Failed to write a VoteRawData for vote " + vote.getId(), e);
    }
  }

  private void writeBallotRawData(List<List<Boolean>> votesByVoter, Ballot ballot) {
    try {
      writer.writeStartElement(BALLOT_RAW_DATA);
      writer.writeElement(BALLOT_IDENTIFICATION, ballot.getId());

      final AtomicLong indexSequence = new AtomicLong();
      final Map<String, VotesCastedReader.Request> requests = ballot.getQuestions().stream()
                                                                    .collect(Collectors.toMap(
                                                                        Question::getId,
                                                                        this::createRequest));

      votesByVoter.stream()
                  .map(selection -> createBallotTag(indexSequence.incrementAndGet(), ballot, requests, selection))
                  .filter(Objects::nonNull)
                  .forEach(this::writeBallotCasted);

      writer.writeEndElement(BALLOT_RAW_DATA);
    } catch (XMLStreamException e) {
      throw ECHGenerationException.forEch0222("Failed to write a BallotRawData for ballot " + ballot.getId(), e);
    }
  }

  private void writeBallotCasted(BallotCasted value) {
    try {
      writer.writeObject(BallotCasted.class, value, BALLOT_CASTED);
    } catch (JAXBException e) {
      throw ECHGenerationException.forEch0222("Failed to write a BallotCasted element", e);
    }
  }

  private static BallotCasted createBallotTag(long index, Ballot ballot,
                                              Map<String, VotesCastedReader.Request> requests,
                                              List<Boolean> selectionVector) {
    boolean hasAnswer = requests.values().stream().anyMatch(r -> r.isAnswered(selectionVector));
    if (hasAnswer) {

      BallotCasted ballotCasted = new BallotCasted();
      ballotCasted.setBallotCastedNumber(BigInteger.valueOf(index));

      ballot.getQuestions().stream()
            .map(q -> EchTagsUtils.createQuestionTag(
                q.getId(),
                requests.get(q.getId()).chosenIndex(selectionVector) + 1) // casted index starts at 1

            ).forEach(ballotCasted.getQuestionRawData()::add);

      return ballotCasted;
    } else {
      return null;
    }
  }

  /**
   * A helper class that encapsulate a Marshaller and the XMLEventWriter to expose a "more readable" control
   */
  private class Writer {
    private final Marshaller     marshaller;
    private final XMLEventWriter xmlEventWriter;

    private Writer(XMLEventWriter xmlEventWriter) {
      this.xmlEventWriter = xmlEventWriter;

      try {
        this.marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      } catch (Exception e) {
        throw ECHGenerationException.forEch0222("Failed to initialize JAXB marshaller", e);
      }
    }

    private void initDocument() throws XMLStreamException {
      xmlEventWriter.add(eventFactory.createStartDocument(StandardCharsets.UTF_8.name(), "1.0"));
      xmlEventWriter.add(eventFactory.createStartElement(ECH_0222_P, ECH_0222_NS, DELIVERY));

      for (Map.Entry<String, String> namespace : NAMESPACES.entrySet()) {
        xmlEventWriter.setPrefix(namespace.getKey(), namespace.getValue());
        xmlEventWriter.add(eventFactory.createNamespace(namespace.getKey(), namespace.getValue()));
      }
    }

    void writeHeaders(HeaderType headers) throws XMLStreamException, JAXBException {
      initDocument();
      writeObject(HeaderType.class, headers, HEADER);
    }

    void writeReportingBody(String reportingAuthority) throws JAXBException {
      ReportingBodyType reportingBody = new ReportingBodyType();
      reportingBody.setReportingBodyIdentification(reportingAuthority);
      reportingBody.setCreationDateTime(LocalDateTime.now());
      writeObject(ReportingBodyType.class, reportingBody, REPORTING_BODY);
    }

    void writeStartElement(String localName) throws XMLStreamException {
      xmlEventWriter.add(
          eventFactory.createStartElement(ECH0222Writer.ECH_0222_P, ECH0222Writer.ECH_0222_NS, localName));
    }

    void writeEndElement(String localName) throws XMLStreamException {
      xmlEventWriter.add(eventFactory.createEndElement(ECH0222Writer.ECH_0222_P, ECH0222Writer.ECH_0222_NS, localName));
    }

    void writeEndDocument() throws XMLStreamException {
      xmlEventWriter.add(eventFactory.createEndDocument());
      xmlEventWriter.flush();
    }

    void writeElement(String elementName, String content) throws XMLStreamException {
      xmlEventWriter.add(eventFactory.createStartElement(ECH_0222_P, ECH_0222_NS, elementName));
      xmlEventWriter.add(eventFactory.createCharacters(content));
      xmlEventWriter.add(eventFactory.createEndElement(ECH_0222_P, ECH_0222_NS, elementName));
    }

    <T> void writeObject(Class<T> declaredType, T value, String elementName) throws JAXBException {
      marshaller.marshal(new JAXBElement<>(new QName(ECH_0222_NS, elementName), declaredType, value), xmlEventWriter);
    }
  }

}
