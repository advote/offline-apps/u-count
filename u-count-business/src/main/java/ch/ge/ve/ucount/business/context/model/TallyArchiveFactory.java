/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.context.model;

import ch.ge.ve.filenamer.archive.TallyArchiveReader;
import ch.ge.ve.model.convert.api.VoterChoiceConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.file.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A {@link TallyArchive} provider.
 * <p>
 * This provides you with a correctly configured {@code TallyArchive}, ready to give you access to its "deserialized"
 * content.
 * </p>
 */
@Service
public class TallyArchiveFactory {

  private final ObjectMapper         objectMapper;
  private final VoterChoiceConverter voterChoiceConverter;

  /**
   * Create a new {@link TallyArchive} factory.
   *
   * @param objectMapper         the object mapper.
   * @param voterChoiceConverter the voter choice converter strategy.
   */
  @Autowired
  public TallyArchiveFactory(ObjectMapper objectMapper, VoterChoiceConverter voterChoiceConverter) {
    this.objectMapper = objectMapper;
    this.voterChoiceConverter = voterChoiceConverter;
  }

  /**
   * Create a new {@link TallyArchive} from the given zipped source.
   *
   * @param source the zipped tally archive file.
   *
   * @return a new instance of {@link TallyArchive}.
   *
   * @throws ch.ge.ve.filenamer.archive.InvalidArchiveException if the path does not denote a regular printer archive
   * @throws IllegalArgumentException                           if the archive name does not match the expected format
   * @throws java.io.UncheckedIOException                       if an I/O error occurs
   */
  public TallyArchive readTallyArchive(Path source) {
    TallyArchiveReader reader = TallyArchiveReader.readFrom(source);
    return new TallyArchive(reader, objectMapper, voterChoiceConverter);
  }
}
