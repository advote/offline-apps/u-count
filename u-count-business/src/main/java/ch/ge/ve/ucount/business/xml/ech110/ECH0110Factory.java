/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110;

import ch.ge.ve.interfaces.ech.eCH0110.v4.Delivery;
import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery;
import ch.ge.ve.javafx.business.xml.HeaderTypeFactory;
import ch.ge.ve.model.convert.model.VoterVotationChoice;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.ucount.business.tally.model.TallyResult;
import ch.ge.ve.ucount.business.xml.ech110.intern.EventResultDeliveryBuilder;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Create {@code Delivery} objects conform to ech0110.
 */
@Component
public class ECH0110Factory {

  private final HeaderTypeFactory headerFactory;
  private final String            reportingAuthority;

  @Autowired
  public ECH0110Factory(HeaderTypeFactory headerFactory,
                        @Value("${ech.reporting.authority.name}") String reportingAuthority) {
    this.headerFactory = headerFactory;
    this.reportingAuthority = reportingAuthority;
  }

  /**
   * Create a new instance of {@link Delivery}, that can be serialized into an XML ech-0110 document.
   *
   * @param ech159Definitions                      definitions of the votation (ech-0159)
   * @param registeredVotersByCountingCircleAndDoi metadata: number of registered voters per counting circle / domain of
   *                                               influence
   * @param voterChoices                           metadata: consolidation of the (ordered) choices represented by each
   *                                               voter ballot
   * @param tallyResult                            decrypted protocol tally (U-Count format)
   *
   * @return a new instance of the XML bean {@code Delivery}, complete with the contest's results
   */
  // Note: Election-related data could be regrouped in an object, eg ElectionContext...
  public Delivery create(List<EventInitialDelivery> ech159Definitions,
                         Map<CountingCircle, Map<String, Long>> registeredVotersByCountingCircleAndDoi,
                         List<VoterVotationChoice> voterChoices,
                         TallyResult tallyResult) {
    Delivery delivery = new Delivery();
    delivery.setDeliveryHeader(headerFactory.createHeader());

    delivery.setResultDelivery(
        EventResultDeliveryBuilder.fromReportingAuthority(reportingAuthority)
                                  .withEch159Definitions(ech159Definitions)
                                  .withRegisteredVoters(registeredVotersByCountingCircleAndDoi)
                                  .withVoterChoices(voterChoices)
                                  .withTallyResult(tallyResult)
                                  .build()
    );

    return delivery;
  }

}
