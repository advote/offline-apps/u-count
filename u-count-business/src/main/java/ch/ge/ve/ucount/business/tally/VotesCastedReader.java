/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.tally;

import ch.ge.ve.model.convert.model.VoterVotationChoice;
import ch.ge.ve.protocol.model.Election;
import com.google.common.base.Verify;
import java.util.List;
import java.util.Optional;

/**
 * Component to read answers expressed by voters.
 * <p>
 * It is specifically designed to prepare a request based on a given {@link VoterVotationChoice} list
 * and request the corresponding value in multiple voters ballots.
 * </p>
 */
public class VotesCastedReader {

  // Indicates a semantically blank answer - should probably be part of AnswerTypeEnum
  //    but I cannot evolve the model-converter for now
  private static final String BLANK = "BLANK";

  private final VoterVotationChoice[] definition;

  /**
   * Instantiates a reader, bound to a specific operation.
   *
   * @param definition definition of the operation as {@code VoterVotationChoice} objects
   */
  public VotesCastedReader(List<VoterVotationChoice> definition) {
    Verify.verify(!definition.isEmpty(), "The operation definition cannot be empty");
    this.definition = definition.toArray(new VoterVotationChoice[definition.size()]);
  }

  /**
   * Prepares a request to get the answer chosen for a specific question.
   *
   * @param voteId     identifier for the vote
   * @param ballotId   identifier for the ballot
   * @param questionId identifier for the question
   *
   * @return a {@code Request} object that can get the expressed answer on multiple voter ballots
   */
  public Request forQuestion(String voteId, String ballotId, String questionId) {
    int firstIndex = findIndexOfFirstChoice(voteId, ballotId, questionId);
    return new Request(definition[firstIndex], firstIndex, definition.length);
  }

  private int findIndexOfFirstChoice(String voteId, String ballotId, String questionId) {
    for (int i = 0; i < definition.length; i++) {
      VoterVotationChoice choice = definition[i];
      if (choice.getVoteId().equals(voteId) && choice.getBallotId().equals(ballotId)
          && choice.getElection().getIdentifier().equals(questionId)) {
        return i;
      }
    }

    throw new IllegalArgumentException(String.format(
        "No voter choice matches voteId=\"%s\", ballotId=\"%s\", questionId=\"%s\"", voteId, ballotId, questionId));
  }


  /**
   * This object encapsulates data so that it can read voter ballots (expressed as selection vectors)
   * and provide information about them.
   */
  public static class Request {
    private final VoterVotationChoice voterChoice;
    private final int                 firstCandidateIndex;
    private final int                 choicesSize;

    private Request(VoterVotationChoice voterChoice, int firstCandidateIndex, int choicesSize) {
      this.voterChoice = voterChoice;
      this.firstCandidateIndex = firstCandidateIndex;
      this.choicesSize = choicesSize;
    }

    /**
     * Get the chosen value for the given selection vector.
     * <p>
     * This method can only be used for question with a "1-out-of-n" selection mechanism.
     * </p>
     *
     * @param selectionVector the list containing all choices for the whole operation
     *
     * @return the value of the chosen answer, based on the {@code AnswerTypeEnum} of the underlying choice
     */
    public Optional<String> chosenValue(List<Boolean> selectionVector) {
      final int chosenIndex = chosenIndex(selectionVector);
      return chosenIndex == -1 ? Optional.empty() :
          Optional.of(voterChoice.getAnswerType().getOptions().get(chosenIndex));
    }

    /**
     * Get the index of the chosen candidate for the given selection vector.
     * <p>
     * This method can only be used for question with a "1-out-of-n" selection mechanism.
     * </p>
     *
     * @param selectionVector the list containing all choices for the whole operation
     *
     * @return the (0-based) index of the chosen answer, or -1 if none is selected
     */
    public int chosenIndex(List<Boolean> selectionVector) {
      Verify.verify(choicesSize == selectionVector.size(),
                    "A valid selection vector should contain %s values, but this one has %s",
                    choicesSize, selectionVector.size());

      final Election election = voterChoice.getElection();
      if (election.getNumberOfSelections() != 1) {
        throw new UnsupportedOperationException(String.format(
            "This method is only applicable for single selection questions, but the question %s authorizes %s answers",
            election.getIdentifier(), election.getNumberOfSelections()));
      }

      final int limitIndex = firstCandidateIndex + election.getNumberOfCandidates();

      for (int current = firstCandidateIndex; current < limitIndex; current++) {
        if (selectionVector.get(current)) {
          return (current - firstCandidateIndex);
        }
      }
      return -1;
    }

    /**
     * Indicates whether or not the answer to that request is considered "blank".
     *
     * @param selectionVector the list containing all choices for the whole operation
     *
     * @return {@code true} if an answer exists for this request and its value is semantically "blank"
     */
    public boolean isBlank(List<Boolean> selectionVector) {
      return this.chosenValue(selectionVector).filter(BLANK::equals).isPresent();
    }

    /**
     * Checks if an answer has been expressed, even a blank one.
     *
     * @param selectionVector the list containing all choices for the whole operation
     *
     * @return {@code true} if an answer exists for this request
     */
    public boolean isAnswered(List<Boolean> selectionVector) {
      return this.chosenIndex(selectionVector) != -1;
    }
  }
}
