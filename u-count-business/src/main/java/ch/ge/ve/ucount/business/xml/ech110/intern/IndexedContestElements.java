/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern;

import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.QuestionInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.TieBreakInformationType;
import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery;
import ch.ge.ve.ucount.business.lang.Either;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Helper class for the EventResultDeliveryBuilder.
 * <p>
 * Indexes VoteInformation and Ballots objects for quick reference.
 * </p>
 */
class IndexedContestElements {

  private final Map<String, EventInitialDelivery.VoteInformation>    voteInformationMap;
  private final Map<BallotKey, BallotType>                           ballotsMap;
  private final Map<BallotKey, Map<String, QuestionInformationType>> questionsMap;
  private final Map<BallotKey, Map<String, TieBreakInformationType>> tieBreaksMap;

  private IndexedContestElements(Map<String, EventInitialDelivery.VoteInformation> voteInformationMap,
                                 Map<BallotKey, BallotType> ballotsMap,
                                 Map<BallotKey, Map<String, QuestionInformationType>> questionsMap,
                                 Map<BallotKey, Map<String, TieBreakInformationType>> tieBreaksMap) {
    this.voteInformationMap = Collections.unmodifiableMap(voteInformationMap);
    this.ballotsMap = Collections.unmodifiableMap(ballotsMap);
    this.questionsMap = questionsMap;
    this.tieBreaksMap = tieBreaksMap;
  }

  /**
   * Indexes a collection of {@code VoteInformation}
   *
   * @param voteInformationList the list to index
   *
   * @return a new instance of {@link IndexedContestElements} referencing the objects from the given list
   */
  public static IndexedContestElements from(List<EventInitialDelivery.VoteInformation> voteInformationList) {
    Map<String, EventInitialDelivery.VoteInformation> voteInformationMap = new HashMap<>();
    Map<BallotKey, BallotType> ballotsMap = new HashMap<>();
    Map<BallotKey, Map<String, QuestionInformationType>> questionsMap = new HashMap<>();
    Map<BallotKey, Map<String, TieBreakInformationType>> tieBreaksMap = new HashMap<>();

    for (EventInitialDelivery.VoteInformation voteInformation : voteInformationList) {
      final String voteIdentification = voteInformation.getVote().getVoteIdentification();
      voteInformationMap.put(voteIdentification, voteInformation);

      for (BallotType ballot : voteInformation.getBallot()) {
        final BallotKey ballotKey = new BallotKey(voteIdentification, ballot.getBallotIdentification());
        ballotsMap.put(ballotKey, ballot);

        indexVariantBallot(questionsMap, tieBreaksMap, ballot.getVariantBallot(), ballotKey);
      }
    }

    return new IndexedContestElements(voteInformationMap, ballotsMap, questionsMap, tieBreaksMap);
  }

  private static void indexVariantBallot(Map<BallotKey, Map<String, QuestionInformationType>> questionsMap,
                                         Map<BallotKey, Map<String, TieBreakInformationType>> tieBreaksMap,
                                         BallotType.VariantBallot variantBallot, BallotKey ballotKey) {
    if (variantBallot != null) {
      for (QuestionInformationType question : variantBallot.getQuestionInformation()) {
        questionsMap.putIfAbsent(ballotKey, new HashMap<>());
        questionsMap.get(ballotKey).put(question.getQuestionIdentification(), question);
      }
      for (TieBreakInformationType tieBreak : variantBallot.getTieBreakInformation()) {
        tieBreaksMap.putIfAbsent(ballotKey, new HashMap<>());
        tieBreaksMap.get(ballotKey).put(tieBreak.getQuestionIdentification(), tieBreak);
      }
    }
  }

  /**
   * Retrieve a VoteInformation by its identifier
   *
   * @param voteIdentification vote identifier
   *
   * @return the indexed VoteInformation object
   *
   * @throws NullPointerException if index not found
   */
  public EventInitialDelivery.VoteInformation getVoteInformation(String voteIdentification) {
    return Objects.requireNonNull(voteInformationMap.get(voteIdentification),
                                  () -> "No VoteInformation indexed with id = \"" + voteIdentification + "\"");
  }

  /**
   * Retrieve a Ballot by its identifiers
   *
   * @param voteIdentification identifier of the containing vote
   * @param ballotId           ballot identifier
   *
   * @return the indexed ballot
   *
   * @throws NullPointerException if index not found
   */
  public BallotType getBallot(String voteIdentification, String ballotId) {
    final BallotKey key = new BallotKey(voteIdentification, ballotId);
    return Objects.requireNonNull(ballotsMap.get(key),
                                  () -> String.format("No Ballot indexed with voteId = \"%s\", ballotId = \"%s\"",
                                                      voteIdentification, ballotId));
  }

  /**
   * Retrieve a question's information (either a regular question or a tiebreak) by its identifiers
   *
   * @param voteId     identifier of the containing vote
   * @param ballotId   identifier of the containing ballot
   * @param questionId question identifier
   *
   * @return a reference to either the QuestionInformation or the TieBreakInformation
   *
   * @throws NullPointerException if neither a question nor a tiebreak is indexed with those values
   */
  public Either<QuestionInformationType, TieBreakInformationType> getQuestionOrTieBreak(
      String voteId, String ballotId, String questionId) {

    final BallotKey key = new BallotKey(voteId, ballotId);
    QuestionInformationType question = questionsMap.getOrDefault(key, Collections.emptyMap()).get(questionId);
    TieBreakInformationType tieBreak = tieBreaksMap.getOrDefault(key, Collections.emptyMap()).get(questionId);
    return Either.ofNullables(question, tieBreak);
  }

  // simple tuple of strings, serving as keys in the Ballots map
  private static class BallotKey {
    private final String voteId;
    private final String ballotId;

    private BallotKey(String voteId, String ballotId) {
      this.voteId = voteId;
      this.ballotId = ballotId;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      BallotKey ballotKey = (BallotKey) o;
      return Objects.equals(voteId, ballotKey.voteId) &&
             Objects.equals(ballotId, ballotKey.ballotId);
    }

    @Override
    public int hashCode() {
      return Objects.hash(voteId, ballotId);
    }
  }

}
