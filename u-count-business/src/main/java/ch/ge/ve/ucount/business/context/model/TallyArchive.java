/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.context.model;

import ch.ge.ve.filenamer.archive.TallyArchiveReader;
import ch.ge.ve.model.convert.api.VoterChoiceConverter;
import ch.ge.ve.model.convert.model.VoterChoice;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.ShuffleProof;
import ch.ge.ve.ucount.business.context.exception.InvalidTallyArchiveException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.common.io.Closer;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * A value object that holds all the tally archive components.
 */
public class TallyArchive {

  private final TallyArchiveReader   reader;
  private final ObjectMapper         objectMapper;
  private final VoterChoiceConverter voterChoiceConverter;

  TallyArchive(TallyArchiveReader reader, ObjectMapper objectMapper, VoterChoiceConverter voterChoiceConverter) {
    this.reader = reader;
    this.objectMapper = objectMapper;
    this.voterChoiceConverter = voterChoiceConverter;
  }

  public TallyArchiveReader getReader() {
    return reader;
  }

  public String getOperationName() {
    return reader.getOperationName();
  }

  public Optional<LocalDateTime> getCreationDate() {
    return reader.getCreationDate();
  }

  private CollectionType listOf(Class<?> elementType) {
    return objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, elementType);
  }

  private MapType mapOfIntegerAnd(Class<?> valueType) {
    return objectMapper.getTypeFactory().constructMapType(HashMap.class, Integer.class, valueType);
  }

  private MapType multimapOfIntegerAnd(Class<?> valueType) {
    final TypeFactory tf = objectMapper.getTypeFactory();
    return tf.constructMapType(HashMap.class, tf.constructSimpleType(Integer.class, new JavaType[0]),
                               tf.constructCollectionType(ArrayList.class, valueType));
  }

  public PublicParameters getPublicParameters() {
    try (InputStream inputStream = reader.getPublicParameters()) {
      return objectMapper.readValue(inputStream, PublicParameters.class);
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read public parameters", e);
    }
  }

  public List<BigInteger> getPrimes() {
    try (InputStream inputStream = reader.getPrimesSource()) {
      return objectMapper.readValue(inputStream, listOf(BigInteger.class));
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read primes", e);
    }
  }

  public Map<Integer, List<Encryption>> getShuffles() {
    try (InputStream inputStream = reader.getShufflesSource()) {
      return objectMapper.readValue(inputStream, multimapOfIntegerAnd(Encryption.class));
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read shuffles", e);
    }
  }

  public Map<Integer, Decryptions> getPartialDecryptions() {
    try (InputStream inputStream = reader.getPartialDecryptionsSource()) {
      return objectMapper.readValue(inputStream, mapOfIntegerAnd(Decryptions.class));
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read partial decryptions", e);
    }
  }

  public List<VotersPerCountingCircle> getCountingCircles() {
    try (InputStream inputStream = reader.getCountingCirclesSource()) {
      return objectMapper.readValue(inputStream, listOf(VotersPerCountingCircle.class));
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read counting circles", e);
    }
  }

  public ElectionSetForVerification getElectionSet() {
    try (InputStream inputStream = reader.getElectionSetForVerificationSource()) {
      return objectMapper.readValue(inputStream, ElectionSetForVerification.class);
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read election set", e);
    }
  }

  // Note : never interpreted ?
  public List<BigInteger> getGenerators() {
    try (InputStream inputStream = reader.getGeneratorsSource()) {
      return objectMapper.readValue(inputStream, listOf(BigInteger.class));
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read generators", e);
    }
  }

  // Note : never interpreted ?
  public Map<Integer, EncryptionPublicKey> getPublicKeyParts() {
    try (InputStream inputStream = reader.getPublicKeyPartsSource()) {
      return objectMapper.readValue(inputStream, mapOfIntegerAnd(EncryptionPublicKey.class));
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read public key parts", e);
    }
  }

  // Note : never interpreted ?
  public Map<Integer, List<Point>> getPublicCredentials() {
    try (InputStream inputStream = reader.getPublicCredentialsSource()) {
      return objectMapper.readValue(inputStream, multimapOfIntegerAnd(Point.class));
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read public credentials", e);
    }
  }

  // Note : never interpreted ?
  public Map<Integer, ShuffleProof> getShuffleProofs() {
    try (InputStream inputStream = reader.getShuffleProofsSource()) {
      return objectMapper.readValue(inputStream, mapOfIntegerAnd(ShuffleProof.class));
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read shuffle proofs", e);
    }
  }

  // Note : never interpreted ?
  public Map<Integer, DecryptionProof> getPartialDecryptionProofs() {
    try (InputStream inputStream = reader.getPartialDecryptionProofsSource()) {
      return objectMapper.readValue(inputStream, mapOfIntegerAnd(DecryptionProof.class));
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read partial decryption proofs", e);
    }
  }

  // Note : never interpreted ?
  public Map<Integer, BallotAndQuery> getBallots() {
    try (InputStream inputStream = reader.getBallotsSource()) {
      return objectMapper.readValue(inputStream, mapOfIntegerAnd(BallotAndQuery.class));
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read ballots", e);
    }
  }

  // Note : never interpreted ?
  public Map<Integer, Confirmation> getConfirmations() {
    try (InputStream inputStream = reader.getConfirmationsSource()) {
      return objectMapper.readValue(inputStream, mapOfIntegerAnd(Confirmation.class));
    } catch (Exception e) {
      throw new InvalidTallyArchiveException("Unable to read confirmations", e);
    }
  }

  
  // ********************************
  // WIP : Access to the operation reference data is not defined yet
  // ********************************

  // Currently we only use it to access a single eCH-159 (as Delivery or InputStream) ...
  public Map<String, Supplier<InputStream>> getOperationReferences() {
    return reader.getOperationReferenceNames().stream().collect(Collectors.toMap(
        Function.identity(),
        name -> () -> reader.getOperationReferenceSource(name)
    ));
  }
  
  public List<VoterChoice> recreateVoterChoices() {
    //noinspection UnstableApiUsage
    try (Closer closer = Closer.create()) {
      final InputStream[] array = reader.getOperationReferenceSources().values().stream()
                                        .peek(closer::register)
                                        .toArray(InputStream[]::new);
      
      return voterChoiceConverter.convertToVoterChoiceList(getElectionSet(), array);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
